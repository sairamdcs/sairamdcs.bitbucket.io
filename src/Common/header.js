import React from 'react';
import Avatar from '../Assets/images/avatar.png';
class Header extends React.Component 
{
	logoutUser = () => {
		this.props.logoutUser();
	}
	loginUser = () => {
		this.props.loginUser();
	}
    render() 
	{
        return ( 
		 	<React.Fragment>
            <div className="top-bar">
                <div className="navbar">
                    <div className="logo" />
                    <div className="nav navbar-nav navbar-right">
						<div className="btn-group avatar">
							<button className="btn btn-danger dropdown-toggle avatar_width" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span className="avatar-holder">
									<img src={Avatar} />
								</span>
							</button>
							<div className="dropdown-menu dropdown-menu-right profile_menu">
								<div className="profile_menu-cont">
                                    <div className="profile_menu-avatar">
                                    	<span className="avatar-holder">
                                    		<img src={Avatar} />
                                    	</span>
                                    </div>
                                    <div className="profile_menu-details">
                                        <div className="profile_menu-name"><span>admin</span></div>
                                        <div className="profile_menu-mail">admin@mail.com</div>
                                    </div>
                                </div>
								
                                <div className="profile_menu-nav">
                                    <div className="right">
                                        <div className="profile-btn logout-btn-color">
                                        	<a href="javascript:void(0)" onClick={this.logoutUser}>
                                        		<i className="ion-power" /> 
                                        		Logout
                                        	</a>
                                        </div>
                                    </div>
                                    <div className="cls" />
                                </div>
							</div>
						</div>
                    </div>
                </div>
            </div>
			</React.Fragment>
        )
    }
}
export default Header;

// <div className="headerbar">
// 					<div className="headerbar-left">
// 						<a href="index.html" className="logo">Offerlly</a>
// 					</div>
// 					{this.props.userData.isAuthenticated === true || this.props.userData.isAuthenticated === "true"?
// 					<div className="headerbar-right" onClick={this.logoutUser} >Logout</div>:null}
// 				</div>