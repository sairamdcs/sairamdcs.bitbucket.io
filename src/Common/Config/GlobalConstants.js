module.exports = Object.freeze({
	//API_HOST:'http://34.206.74.46:8000',
	API_HOST:'http://agri.divergentconsulting.co.in:8000',
	PER_PAGE:20,
	PAGINATIONS:5,
	LAND_BASE_TYPES:[{value:"OWN",label:"Own"},{label:"Leased",value:"LEASE"}],
    QUANTITY_UOM :[{value:"KG",label:"KG"},{value:"QUINTAL",label:"Quintal"},{value:"LITERS",label:"Liters"}],
	DISTANCE_UOM :[{value:"INCH",label:"Inch"},{value:"FEET",label:"Feet"},{value:"METER",label:"Meter"}],
	AREA_UOM : [{value:"ACRE",label:"Acre"},{value:"HECTOR",label:"Hector"}],
	CROP_SEASONS : [{value:"RABI",label:"Rabi"},{value:"KHARIF",label:"Kharif"}],
	IRRIGATION_TYPES : [{value:"RAIN",label:"Rain"},{value:"WELL",label:"Well/Borewell"},{value:"DRIP",label:"Drip"},{value:"CANAL",label:"Canal"},{value:"LAKE",label:"Lake"},{value:"RIVER",label:"River"}],
	SOIL_TYPES : [{value:"BLACK",label:"Black"},{value:"ALLUVIAL",label:"Alluvial"},{value:"BLACK",label:"Black"},{value:"DESERT",label:"Desert"},{value:"LATERITE",label:"Laterite"}],
	CROP_GRADE:[{label:'A',value:'A'},{label:'B',value:'B'}],
	IS_MACHINARY:[{label:'Yes',value:'YES'},{label:'No',value:'NO'}],
	IMPACT_LEVEL:[{label:'Good',value:'GOOD'},{label:'High',value:'HIGH'}],
	DISASTER_TYPE:[{label:'Flood',value:'FLOOD'},{label:'Heavy Rain',value:'HEAVYRAIN'}],
	CROP_AGE:[{label:'Age 1',value:'AGE1'},{label:'Age 2',value:'AGE2'},{label:'Age 3',value:'AGE3'}],
	CROP_CONDITION:[{label:'Stemmed',value:'STEMMED'},{label:'De-Stemmed',value:'DESTEMMED'}],
	CROP_INIATION_TYPE:[{label:'Seeding',value:'SEEDING'},{label:'Transplantation',value:'TRANSPLANTATION'}],
	WAREHOUSE_TRANSACTION_TYPE:[{label:'Out',value:'OUT'},{label:'In',value:'IN'}]
});