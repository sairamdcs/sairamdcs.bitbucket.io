import React from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
export class MapContainer extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      islocationLoaded : false,
      location:{}
    }
  }
  componentDidMount = () => {
    this.getLocation()
    .then((currentLoc) => {
      this.setState({location:currentLoc,islocationLoaded:true});
    })
  .catch(err => console.log('Location error:' + err))
  }

  getLocation = (onSuccess, onFail) => {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        this.props.locationHandler(pos);
        resolve(pos);
        });
    } 
  })
}

  render = () => {
    var renderhtml = '';
    if (this.state.islocationLoaded && this.state.location){
      renderhtml = <Map 
              google={this.props.google} 
              zoom={14}
              initialCenter={this.state.location}
              >
              <Marker onClick={this.onMarkerClick}
                      name={'Current location'} />
            </Map>;
    }
    return (
          <React.Fragment>
            {renderhtml}
          </React.Fragment>
        );
  }
}
export default GoogleApiWrapper({
  apiKey: ('AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes')
})(MapContainer)
