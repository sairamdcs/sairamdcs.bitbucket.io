import React from 'react';
class PrimaryButton extends React.Component{
    render(){
        return (  
            <React.Fragment >
                <button 
                	type={this.props.type?this.props.type:"button"}
                	disabled={this.props.disabled}
                	onClick={this.props.onClickHandler}
                	className="btn btn-primary">{this.props.btnText?this.props.btnText:'Save'}</button>
            </React.Fragment>
        )
    }
}
export default PrimaryButton;