import React from 'react';
import firebase from "firebase";
import FileUploader from "react-firebase-file-uploader";
class TextBox extends React.Component{
    render(){
        let label=this.props.fieldData.label;
        let fieldName=this.props.fieldData.fieldName;
        let value=this.props.fieldData.value;
        let errormsg=this.props.fieldData.errorMsg;
        let isValid = this.props.fieldData.isValid;
        let i = this.props.dataIndex;
        return (  
            <React.Fragment >
                <label htmlFor={fieldName} className="placeholder">{label}</label>
                <FileUploader
                    accept="image/*"
                    name={fieldName}
                    id={fieldName}
                    randomizeFilename
                    storageRef={firebase.storage().ref("images")}
                    onUploadStart={this.props.handleUploadStart}
                    onUploadError={this.props.handleUploadError}
                    onUploadSuccess={this.props.handleUploadSuccess}
                    onProgress={this.props.handleProgress}
          />
                {isValid?'':<span className="errorMsg">{errormsg}</span>}
            </React.Fragment>
        )
    }
}
export default TextBox;