import React from 'react';
class SecButton extends React.Component{
    render(){
        return (  
            <React.Fragment >
                <button 
                    type="button"
                    onClick={this.props.onClickHandler}
                    className="btn btn-secondary">{this.props.btnText?this.props.btnText:'Cancel'}</button>
            </React.Fragment>
        )
    }
}
export default SecButton;