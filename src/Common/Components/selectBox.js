import React from 'react';
import { Dropdown } from 'semantic-ui-react';
class SelectBox extends React.Component{
    render(){
        let label=this.props.fieldData.label;
        let fieldName=this.props.fieldData.fieldName;
        let errormsg=this.props.fieldData.errorMsg;
        let isValid = this.props.fieldData.isValid;
        let i = this.props.dataIndex;
        let isMultiSelect = this.props.multiSelect;
        return (  
            <React.Fragment>
                <label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
                <Dropdown
                    accessKey={i}
                    placeholder={this.props.fieldData.label}
                    fluid
                    search
                    multiple = {isMultiSelect}
                    selection
                    onChange={this.props.updateHandler} 
                    options={this.props.fieldData.dropOptions}
                    defaultValue={this.props.fieldData.value}
                  />
                {isValid?'':<span className="errorMsg">{errormsg}</span>}
            </React.Fragment>
        )
    }
}
export default SelectBox;