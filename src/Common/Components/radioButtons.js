
import React from 'react';
class RadioGroup extends React.Component{
    render(){
        let label=this.props.fieldData.label;
        let fieldName=this.props.fieldData.fieldName;
        let value=this.props.fieldData.value;
        let pattern=this.props.fieldData.validPattern;
        let errormsg=this.props.fieldData.errorMsg;
        let isValid = this.props.fieldData.isValid;
        let maxLength = this.props.fieldData.maxLength?this.props.fieldData.maxLength:200;
        let i = this.props.dataIndex;
        return (  
            <React.Fragment>
                <label htmlFor={fieldName} className="placeholder">{label}</label>
                <div className="radio-group">
                {this.props.fieldData.options.map(option => {
                  return (
                    <label key={option.value}>
                      <input
                        accessKey={i}
                        //className={this.props.fieldData.classNames}
                        name={this.props.fieldData.name}
                        onChange={this.props.updateHandler}
                        value={option.value}
                        checked={option.value===value?'checked':false}
                        type="radio" /> {option.label}
                    </label>
                  );
                })}
                {isValid?'':<span className="errorMsg">{errormsg}</span>}
                </div>
            </React.Fragment>
        );
    }
}
export default RadioGroup;
