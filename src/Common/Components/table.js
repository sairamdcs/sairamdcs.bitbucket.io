import ReactDOM from 'react-dom';
import React from 'react';
import Pagination from "react-js-pagination";
class Table extends React.Component{
	constructor(props){
	    super(props);
	    this.state = {
			perPage:global.Service.GC.PER_PAGE,
			activePage:1,
            pageRangeDisplayed:global.Service.GC.PAGINATIONS,
			totalRec:this.props.data.length,
			data:this.props.data
	    };
	}
	
	handlePageChange = (pageNumber) => {
		this.setState({activePage: pageNumber});
	}
	generateHeaders = () => {
        var cols = this.props.columns;
        return cols.map((colData) => {
        	if(colData.checked){
            return <th key={colData.key} >
                    <div>
                    <div data-key={colData.key} className="mblHide" data-sort="ASC" onClick={this.props.sortClickHandler}>{colData.label}</div></div>
	            {colData.displayFilter?
	            	<div className="tableHeadFilterSection">
                    <div className="filterInput">
                    <input type="text" placeholder={colData.label} name={colData.key} className="form-control" onChange={this.props.onChangeHandler} /></div>
                    </div>
	            :''}
            	</th>;
            } else {
            	return '';
            }
        });
	}

	generateRows = () => {
        var cols = this.props.columns;
        var data = this.props.data;
        return data.map((item,index) => {
        	var cells;
        	let startNumber = (this.state.activePage -1) * this.state.perPage;
        	let stopNumber = startNumber + this.state.perPage;
        	if (index >= startNumber && index<stopNumber){ 
            cells = cols.map( (colData) => {
            	if(colData.checked){
                    if (colData.popupDisplay){
                        return <td key={colData.key} className="headcol">
                        <span className="tableOptionPopup" onClick={(e)=>this.props.popupClickHandler(e,item)} >
                            {/*<span className="mblHide">{colData.key}</span>*/}
                            <span className="option"><img src="/images/activites.jpg" width="30" /></span>
                        </span>
                        </td>;
                    } else {
            			return <td key={colData.key}> {item[colData.key]} </td>;
            		}
            	} else {
            		return '';
            	}
            });
        	}
            return <tr key={index}>{cells}</tr>;
        });
    }

    generateFilterFieldComponents = () => {
    	let _this = this;
    	var cols = this.props.columns;
        return cols.map(function(colData,index) {
        	return <label className="checkBoxLabel" key={index}> <input type="checkbox" className="checkbox" name={colData.key} checked={colData.checked} onChange={_this.props.checkboxHandler} />{colData.label} </label>;
    	});
    }

  	render(){
        var rowComponents = this.generateRows();
        var headerComponents = this.generateHeaders();
        var filterFieldComponents = this.generateFilterFieldComponents();
        return (
   				<React.Fragment>
				  	<div className="tableSection">
	                    
                        <div className=
                            {this.props.displaySideFilter?
                                "tableDataSection tableDataSectionWidth table-responsive":
                                "tableDataSection table-responsive"
                            }>
	                      <table className="react-table">
	                      <thead><tr>{headerComponents}</tr></thead>
	                      <tbody>{rowComponents}</tbody>
	                      </table>
	                    </div>
                        {this.props.displaySideFilter?
	                    <div className="tableFilterSection">{filterFieldComponents}</div>
                        :null}
	              </div>
	              <div className="text-center pagination-section">
	              <Pagination
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.perPage}
                      totalItemsCount={this.state.totalRec}
                      pageRangeDisplayed={this.state.pageRangeDisplayed}
                      onChange={this.handlePageChange}
                    />
                    </div>
				  </React.Fragment>
				);
	}
};


export default Table;