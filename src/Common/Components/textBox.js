import React from 'react';
class TextBox extends React.Component{
    render(){
        let label=this.props.fieldData.label;
        let fieldName=this.props.fieldData.fieldName;
        let value=this.props.fieldData.value;
        let pattern=this.props.fieldData.validPattern;
        let errormsg=this.props.fieldData.errorMsg;
        let isValid = this.props.fieldData.isValid;
        let maxLength = this.props.fieldData.maxLength?this.props.fieldData.maxLength:200;
        let i = this.props.dataIndex;
        return (  
            <React.Fragment >
                <input type={this.props.fieldData.type?this.props.fieldData.type:"text"}
                onChange={this.props.updateHandler} 
                name={fieldName} 
                id={fieldName}
                className={isValid?"form-control":"form-control inputError"} 
                datapattern={pattern}
                accessKey={i}
                maxLength={maxLength}
                autoComplete="off"
                value={value} required />
                <label htmlFor={fieldName} className="form-control-placeholder">{label}</label>
                {isValid?'':<span className="errorMsg">{errormsg}</span>}
            </React.Fragment>
        )
    }
}
export default TextBox;