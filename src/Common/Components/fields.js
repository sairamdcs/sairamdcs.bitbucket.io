import React from 'react';
import { Dropdown } from 'semantic-ui-react';
// import GoogleLocation from './GoogleLocation.js';
import Radio from './radioButtons.js';
class Fields extends React.Component{
  	render(){
  		let fieldsPerRow = 2;
  		if(this.props.fieldsPerRow)
  			fieldsPerRow = this.props.fieldsPerRow;
  		let label=this.props.fieldData.label;
  		let fieldName=this.props.fieldData.fieldName;
  		let value=this.props.fieldData.value;
  		let pattern=this.props.fieldData.validPattern;
  		let errormsg=this.props.fieldData.errorMsg;
  		let fieldType = this.props.fieldData.fieldType;
  		let i = this.props.dataIndex;
  		let isValid = this.props.fieldData.isValid
  		var field = '';
  		var classNames = "form-group col-md-6";
  		if (fieldsPerRow == 1){
  			classNames = "form-group col-md-12";
  		}
  		if (fieldsPerRow == 2){
  			classNames = "form-group col-md-6";
  		}
  		if (fieldsPerRow == 3){
  			classNames = "form-group col-md-4";
  		}
  		switch (fieldType){
  			case 'TEXTBOX':
  				field = <React.Fragment>
	  						<div className={classNames} >
						        <input type="text" 
						  			onChange={this.props.updateHandler} 
						  			name={fieldName} 
						  			id={fieldName}
						  			className={isValid?"form-control":"form-control inputError"} 
						  			datapattern={pattern}
						  			accessKey={i}
						  			autoComplete="off"
						  			value={value} required />
						        <label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
						        {isValid?'':<span className="errorMsg">{errormsg}</span>}
						    </div>
					  	</React.Fragment>;
  			break;
  			case 'SELECTBOX':
  			var options = '';
  			
	  			if(this.props.fieldData.dropOptions){
	  				options = this.props.fieldData.dropOptions.map((item,index) =>{
				  				return <option value={item.value} key={index}>{item.label}</option>;
				  			});
		  		} else {
		  			options = '';
		  		}

  				field = <React.Fragment>
	  						<div className={classNames}>
						        <select
						  			onChange={this.props.updateHandler} 
						  			name={fieldName} 
						  			id={fieldName}
						  			className={isValid?"form-control":"form-control inputError"}
						  			accessKey={i}
						  			value={value} required >
						  			{options}
						  			</select>
						        <label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
						        {isValid?'':<span className="errorMsg">{errormsg}</span>}
						    </div>
					  	</React.Fragment>;
  			break;
  			case 'TEXTAREA':
  				field = <React.Fragment>
	  						<div className="col-md-6" >
	  						<label htmlFor={fieldName}>{label}</label>
						        
						        <textarea 
						  			onChange={this.props.updateHandler} 
						  			name={fieldName} 
						  			id={fieldName}
						  			className={isValid?"form-control":"form-control inputError"}
						  			accessKey={i}
						  			value={value}
						  			>{value}
						  			</textarea>
						        {isValid?'':<span className="errorMsg">{errormsg}</span>}
						    </div>
					  	</React.Fragment>;
  			break;
  			// case 'MAP_LOCATION':
  			// 	field = <React.Fragment>
	  		// 				<div className="col-md-12 pb-4">
	  		// 				<label htmlFor={fieldName}>{label}</label>
	  		// 				    <div className="googleLocationDiv" id="google_map" >
					// 	        	<GoogleLocation locationHandler={this.props.locationHandler} />
					// 	        </div>
					// 	        {isValid?'':<span className="errorMsg">{errormsg}</span>}
					// 	    </div>
					//   	</React.Fragment>;
  			// break;
  			case 'DATEBOX':
  			field = <React.Fragment>
	  						<div className={classNames} >
						        <input type="date" 
						  			onChange={this.props.updateHandler} 
						  			name={fieldName} 
						  			id={fieldName}
						  			className={isValid?"form-control":"form-control inputError"}
						  			accessKey={i}
						  			value={value} />
						        <label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
						        {isValid?'':<span className="errorMsg">{errormsg}</span>}
						    </div>
					  	</React.Fragment>;
  			break;
  			case 'SINGLESELECT':
  			field = <React.Fragment>
  						<div className={classNames} >
  						<label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
  						<Dropdown
  							accessKey={i}
						    placeholder={this.props.fieldData.label}
						    fluid
						    search
						    selection
						    onChange={this.props.onMultiSelectChangeHandler} 
						    options={this.props.fieldData.dropOptions}
						  />
						
					        {isValid?'':<span className="errorMsg">{errormsg}</span>}
					    </div>
				  	</React.Fragment>;
				  	break;
  			
  			case 'MULTISELECT':
  			field = <React.Fragment>
  						<div className={classNames} >
  						<label className="form-control-placeholder" htmlFor={fieldName}>{label}</label>
  						<Dropdown
  							accessKey={i}
						    placeholder={this.props.fieldData.label}
						    fluid
						    multiple
						    search
						    selection
						    onChange={this.props.onMultiSelectChangeHandler} 
						    options={this.props.fieldData.dropOptions}
						  />
						
					        {isValid?'':<span className="errorMsg">{errormsg}</span>}
					    </div>
				  	</React.Fragment>;
				  	break;
			case 'RADIOBUTTON':
				field = <Radio 
							options={this.props.fieldData.options} 
							classNames={classNames}  
							onChange={this.props.updateHandler} 
							name={fieldName} 
							label={label}
							errormsg={errormsg}
							accessKey={i}
							/>;
							break;
  		}

    	return (
   				<React.Fragment>
				  	{field}
				  </React.Fragment>
				);
	}
};
export default Fields;