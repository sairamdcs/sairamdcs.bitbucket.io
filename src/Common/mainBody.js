import React from 'react';
import { Route} from 'react-router-dom';
import Login from '../Modules/Login/index.js';
import Signup from '../Modules/Signup/index.js';
import Dashboard from '../Modules/Dashboard/index.js';
import UsersManagement from '../Modules/UsersManagement/index.js';
import StoresManagement from '../Modules/StoresManagement/index.js';
import OffersManagement from '../Modules/OffersManagement/index.js';
import PeopleNearMe from '../Modules/PeopleNearMe/index.js';
import * as ROUTES from './Config/RouteConfig.js';
import Services from '../Services/index.js';
const Service = new Services();
global.Service = Service;
class MainBody extends React.Component 
{
    render() {
        return ( 
        	<React.Fragment>
        		<Route path={ROUTES.DASHBOARD} component={Dashboard} />
				<Route path={ROUTES.USER_MANAGEMENT} component={UsersManagement} />
				<Route path={ROUTES.STORES_MANAGEMENT} component={StoresManagement} />
				<Route path={ROUTES.OFFERS_MANAGEMENT} component={OffersManagement} />
				<Route path={ROUTES.PEOPLE_NEAR_ME} component={PeopleNearMe} />
        	</React.Fragment>
        )
    }
}
export default MainBody;