import React from 'react';

class Footer extends React.Component{
  	render(){
    	return (
    		<div className="footer row" id="footer-bar" >
                <div className="col-md-12 nopad-right copyrights">
                    <div className="text"><span>Copyright © 2019 Divergent. All rights reserved.</span></div>
                </div>
            </div>
    	);
	}
};
export default Footer;