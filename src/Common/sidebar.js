import React from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../Common/Config/RouteConfig.js';

const Navigation = () => (
  
      <React.Fragment>
       <div className="menu-bar-mobile" id="open-left">
          <a href="javascript:void(0)">
                <div className="mob-nav-icon">
                    <span />
                    <span />
                    <span />
                </div>
            </a>
        </div>       
                   
        <div className="left-bar" id="left-bar">
                <div className="showLeftPush">
                  <a href="javascript:void(0)" className="menu-bar pull-right">
                      <span>Navigation</span>
                      <div className="nav-icon">
                            <span />
                            <span />
                            <span />
                        </div>
                  </a>
                </div>
                <ul className="list-unstyled menu-parent" id="mainMenu">
                    <li>
                        <Link className="waves-effect waves-light" to={ROUTES.DASHBOARD}>
                            <i className="fa fa-tachometer" />
                            <span className="text">Dashboard</span>
                        </Link>
                    </li>
                    <li>
                        <Link to={ROUTES.USER_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-users" /><span className="text">User Management</span>
                        </Link>
                    </li>
                    <li>
                        <Link to={ROUTES.STORES_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-sitemap" /><span className="text">Branches Management</span>
                        </Link>
                    </li>
                    <li>
                        <Link to={ROUTES.OFFERS_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-tags" /><span className="text">Offers Management</span>
                        </Link>
                    </li>
                    
                    <li>
                        <Link to={ROUTES.PEOPLE_NEAR_ME} className="waves-effect waves-light" >
                        <i className="fa fa-street-view" /><span className="text">People Near Me</span>
                        <i className="chevron ti-angle-right"></i></Link></li>
                    {/*<li>
                        <Link to={ROUTES.USER_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-user" /><span className="text">Account Summary</span></Link></li>
                    <li>
                        <Link to={ROUTES.USER_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-question-circle-o" />
                        <span className="text">FAQ</span><i className="chevron ti-angle-right"></i></Link></li>
                    <li>
                        <Link to={ROUTES.USER_MANAGEMENT} className="waves-effect waves-light" >
                        <i className="fa fa-commenting-o" />
                    <span className="text">Premium Push Notifications</span>
                    <i className="chevron ti-angle-right"></i></Link></li>*/}
                </ul>
            </div>
           </React.Fragment>
);

export default Navigation;
