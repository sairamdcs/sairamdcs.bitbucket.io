module.exports = Object.freeze({
	SIGNUP:'/seller/signup',
	LOGIN:'/seller/signin',
	REGISTERED_LIST:'',
	SELLER_OFFERS:'/offers',
	SELLER_STORES:'/stores'
});