class storeServicesResponseBuliders {
	
	getSellerStores = (resObj) => {
		var storesData = [];
		const keys = Object.keys(resObj);
		const vals = Object.values(resObj);
		for (var i=0;i<keys.length;i++) {
		  const storeData = Object.assign(vals[i], {id:keys[i]});
		  storesData.push(storeData);
		}
		return storesData;
	};

	addStore = (resObj) => {
		return {'status':'success','data':resObj};
	};
	deleteSellerStore = (resObj) => {
		return resObj;
	}

}
export default storeServicesResponseBuliders;