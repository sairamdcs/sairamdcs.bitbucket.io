class responseBuliders {
	addNewFarmerResponseBuilder = (dataobj,fId) => {
		Object.assign(dataobj,{farmerID:fId});
			const data = {
				'farmerID':fId,
				'firstName':dataobj.basicInfo.firstName,
				'lastName':dataobj.basicInfo.lastName,
				'mobileNumber':dataobj.basicInfo.mobileNumber,
				'cropName':dataobj.cropName,
				'cropStartDate':this.standardDateFormat(dataobj.cropStartDate),
				'expectedHarvestDate':this.standardDateFormat(dataobj.expectedHarvestDate),
				'expectedYield':dataobj.expectedYield,
				'landBase':dataobj.landBase,
				'options':'Options'
			}

		return {'status':'success','data':data};
	};
	farmersBasicInfo = (data) => {
		return data.map((item,index) => {
			return {
				'farmerID':item._id,
				'firstName':item._source.basicInfo.firstName,
				'lastName':item._source.basicInfo.lastName,
				'mobileNumber':item._source.basicInfo.mobileNumber,
				'cropName':item._source.cropName,
				'cropStartDate':this.standardDateFormat(item._source.cropStartDate),
				'expectedHarvestDate':this.standardDateFormat(item._source.expectedHarvestDate),
				'expectedYield':item._source.expectedYield,
				'landBase':item._source.landBase,
				'options':'Options'
			}
		});
	};
	addFarmerCropResponseBuilder = (thisFarmerObj) => {
		return {'status':'success','data':thisFarmerObj};
	};

	addLandPreparationInfoResponseBuilder = () => {
		return {'data':'success'};
	};

	standardDateFormat = (obj) => {
		if(Object.keys(obj).length === 0){
			return '';
		} else {
			var dateObj = new Date(obj);
			var month = dateObj.getUTCMonth() + 1; //months from 1-12
			var day = dateObj.getUTCDate();
			var year = dateObj.getUTCFullYear();
			return day + "-" + month + "-" + year ;
		}
			
	}
}
export default responseBuliders;