class offerServicesResponseBuliders {
	
	getSellerOffers = (resObj) => {
		var offersData = [];
		const keys = Object.keys(resObj);
		const vals = Object.values(resObj);
		for (var i=0;i<keys.length;i++) {
		  const offerData = Object.assign(vals[i], {id:keys[i]});
		  offersData.push(offerData);
		}
		return offersData;
	};

	addOffer = (resObj) => {
		return {'status':'success','data':resObj};
	};
	deleteSellerOffer = (resObj) => {
		return resObj;
	}

}
export default offerServicesResponseBuliders;