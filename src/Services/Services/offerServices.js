import requestBuilders from '../RequestBuilders/offerServices.js';
import responseBuilders from '../ResponseBuilders/offerServices.js';
class OfferServices {
	constructor (Service,AEP){
		this.appServices = Service;
		this.AEP = AEP;
		this.requestBuilders = new requestBuilders();
		this.responseBuilders = new responseBuilders();
	}

	getSellerOffers = (dataObj) => {
		//const dataobj = this.requestBuilders.getSellerOffers(dataObj);
		const url = this.AEP.SELLER_OFFERS;
		return new Promise((resolve, reject) => {
			this.appServices.getData(url)
			.then ((res)=>{
				resolve(this.responseBuilders.getSellerOffers(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	};

	addOffer = (dataObj) => {
		const dataobj = this.requestBuilders.addOffer(dataObj);
		const url = this.AEP.SELLER_OFFERS;
		return new Promise((resolve, reject) => {
			this.appServices.postData(url,dataobj)
			.then ((res)=>{
				resolve(this.responseBuilders.addOffer(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	};
	
	deleteSellerOffer = (id) => {
		//const dataobj = this.requestBuilders.deleteSellerOffer(dataObj);
		const url = this.AEP.SELLER_OFFERS+'/'+id;
		return new Promise((resolve, reject) => {
			this.appServices.deleteData(url)
			.then ((res)=>{
				resolve(this.responseBuilders.deleteSellerOffer(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	}
	updateOffer = (id,reqObj) => {
		const dataobj = this.requestBuilders.addOffer(reqObj);
		console.log(id);
		const url = this.AEP.SELLER_OFFERS+'/'+id;
		return new Promise((resolve, reject) => {
			this.appServices.patchData(url,dataobj)
			.then ((res)=>{
				resolve(this.responseBuilders.addOffer(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	}
	


}
export default OfferServices;