import axios from 'axios';
import requestBuilders from '../RequestBuilders/loginRegServices.js';
import responseBuilders from '../ResponseBuilders/loginRegServices.js';
class storeServices {
	constructor (Service,AEP){
		this.appServices = Service;
		this.AEP = AEP;
		this.requestBuilders = new requestBuilders();
		this.responseBuilders = new responseBuilders();
	}

	registerNew = (dataObj) => {
		const dataobj = this.requestBuilders.registerNew(dataObj);
		const url = this.AEP.SIGNUP;
		return this.appServices.postData(url,dataobj)
		.then ((res)=>{
			return this.responseBuilders.registerNew(res)
		})
		.catch((errorObj)=>{
			console.log(errorObj);
		});
	};
	


}
export default storeServices;