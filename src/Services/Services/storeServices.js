
import requestBuilders from '../RequestBuilders/storeServices.js';
import responseBuilders from '../ResponseBuilders/storeServices.js';
class StoreServices {
	constructor (Service,AEP){
		this.appServices = Service;
		this.AEP = AEP;
		this.requestBuilders = new requestBuilders();
		this.responseBuilders = new responseBuilders();
	}

	getSellerStores = (reqObj) => {
		//const dataobj = this.requestBuilders.getSellerStores(reqObj);
		const url = this.AEP.SELLER_STORES;
		return new Promise((resolve, reject) => {
			this.appServices.getData(url)
			.then ((res)=>{
				resolve(this.responseBuilders.getSellerStores(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	};

	addStore = (reqObj) => {
		const dataobj = this.requestBuilders.addStore(reqObj);
		const url = this.AEP.SELLER_STORES;
		return new Promise((resolve, reject) => {
			this.appServices.postData(url,dataobj)
			.then ((res)=>{
				resolve(this.responseBuilders.addStore(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	};
	
	deleteSellerStore = (id) => {
		//const dataobj = this.requestBuilders.deleteSellerStore(dataObj);
		const url = this.AEP.SELLER_STORES+'/'+id;
		return new Promise((resolve, reject) => {
			this.appServices.deleteData(url)
			.then ((res)=>{
				resolve(this.responseBuilders.deleteSellerStore(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	}
	updateStore = (id,reqObj) => {
		const dataobj = this.requestBuilders.addStore(reqObj);
		console.log('storeId',id);
		const url = this.AEP.SELLER_STORES+'/'+id;
		return new Promise((resolve, reject) => {
			this.appServices.patchData(url,dataobj)
			.then ((res)=>{
				resolve(this.responseBuilders.addStore(res));
			})
			.catch((errorObj)=>{
				reject(errorObj);
			});
		});
	}
	


}
export default StoreServices;