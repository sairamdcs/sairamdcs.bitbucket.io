import storeServices from './Services/storeServices.js';
import userServices from './Services/userServices.js';
import offerServices from './Services/offerServices.js';
import loginRegServices from './Services/loginRegServices.js';
import Services from './Services.js';
import ApiEndPoints from './apiEndPoints.js';
import validateFields from '../Validations/validateFields.js';

class AppServices {
	constructor (){
		const Service = new Services();
		const AEP = ApiEndPoints;
		this.storeServices = new storeServices(Service,AEP);
		this.offerServices = new offerServices(Service,AEP);
		this.userServices = new userServices(Service,AEP);
		this.loginRegServices = new loginRegServices(Service,AEP);
		this.validateFields = new validateFields(Service,AEP);
	}
	validateUserLogin = (reqObj) => {
		return this.userServices.validateUserLogin(this.getKeyValuePairFromObj(reqObj));
	}
	registerNew = (reqObj) => {
		return this.loginRegServices.registerNew(this.getKeyValuePairFromObj(reqObj));	
	}
	
	//################ OFFERS MANAGEMENT APIS #########################3
	getSellerOffers = (reqObj) => {
		return this.offerServices.getSellerOffers(reqObj);
	}
	addOffer = (reqObj) => {
		return this.offerServices.addOffer(this.getKeyValuePairFromObj(reqObj));
	}
	deleteSellerOffer = (offerId) => {
		return this.offerServices.deleteSellerOffer(offerId);
	}
	updateOffer = (offerId,reqObj) => {
		return this.offerServices.updateOffer(offerId,this.getKeyValuePairFromObj(reqObj));
	}
	//################ OFFERS MANAGEMENT APIS #########################3

	//################ STORES MANAGEMENT APIS #########################3
	getSellerStores = (reqObj) => {
		return this.storeServices.getSellerStores(reqObj);
	}
	addStore = (reqObj) => {
		return this.storeServices.addStore(this.getKeyValuePairFromObj(reqObj));
	}
	deleteSellerStore = (storeId) => {
		return this.storeServices.deleteSellerStore(storeId);
	}
	updateStore = (storeId,reqObj) => {
		return this.storeServices.updateStore(storeId,this.getKeyValuePairFromObj(reqObj));
	}
	//################ STORES MANAGEMENT APIS #########################3

	getKeyValuePairFromObj = (reqObj) =>{
		if(reqObj !== undefined){
			var newObj = {};
			reqObj.forEach (function(item){
				let inobj = {
					  [item.fieldName]: item.value,
					};
				Object.assign(newObj,inobj);
			});
			return newObj;
		}
	}
}
export default AppServices;