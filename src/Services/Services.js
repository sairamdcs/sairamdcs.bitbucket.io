import axios from 'axios';
const TOKEN = localStorage.getItem('token');
const HEADERS = {headers: {'Authorization':'Bearer '+TOKEN}};
class Services {
	constructor () {
		this.host = 'https://us-central1-offerly-4bc72.cloudfunctions.net/app';
		axios.defaults.headers.common['Authorization'] = HEADERS;
	}
	getData = (url) => {
		this.addSpinner();
		const fullUrl = this.getCompleteUrl(url);
		return new Promise((resolve, reject) => {
			return axios.get(fullUrl,HEADERS)
		    .then((response) => {
		    	this.removeSpinner();
		    	resolve(response.data);
		    })
		    .catch((error)=>{
		    	if(error.response.status === 403)
		    		window.location.href = '/login';
		       	this.removeSpinner();
		        reject(error);
		    });
		});
	}

	postData = (url,dataobj) => {
		this.addSpinner();
		console.log('dataobj in service.js',dataobj);
		const fullUrl = this.getCompleteUrl(url);
		return new Promise((resolve, reject) => {
			return axios.post(fullUrl,dataobj,HEADERS)
		    .then((response) => {
		    	this.removeSpinner();
		    	resolve(response.data);
		    })
		    .catch((error)=>{
		    	this.removeSpinner();
		       	reject(error);
		    });
		});
	}
	deleteData = (url) => {
		this.addSpinner();
		const fullUrl = this.getCompleteUrl(url);
		return new Promise((resolve, reject) => {
			return axios.delete(fullUrl,HEADERS)
		    .then((response) => {
		    	this.removeSpinner();
		    	resolve(response.data);
		    })
		    .catch((error)=>{
		       	this.removeSpinner();
		        reject(error);
		    });
		});
	}
	patchData = (url,dataobj) => {
		this.addSpinner();
		const fullUrl = this.getCompleteUrl(url);
		
		return new Promise((resolve, reject) => {
			return axios.patch(fullUrl,dataobj,HEADERS)
		    .then((response) => {
		    	this.removeSpinner();
		    	resolve(response.data);
		    })
		    .catch((error)=>{
		    	this.removeSpinner();
		       	reject(error);
		    });
		});
	}
	
	getCompleteUrl = (url) => {
		return this.host+url;
	}

	addSpinner = () => {
		var div = document.createElement('div');
	    div.className = 'loading';
	    div.setAttribute("id", "spinner");
	    div.innerHTML = 'Loading&#8230;';
	    document.getElementById('root').appendChild(div);
	}

	removeSpinner = () => {
		var spinner = document.getElementById("spinner");
	    if(spinner){
	    	document.getElementById("spinner").remove();
	    }
	}
	
}
export default Services;