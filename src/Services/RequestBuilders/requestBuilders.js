import farmerModel from './farmerModel'
class RequestBuliders {
	addNewFarmerRequestBuilder = (obj) => {
		const dataObj = this.getKeyValuePairFromObj(obj);
		return {
			basicInfo:{
				firstName:dataObj.firstName,
				lastName:dataObj.lastName,
				mobileNumber:dataObj.mobileNumber,
				ownLandBase:dataObj.ownLand,
				leaseLandBase:dataObj.leaseLand,
				mailingAddress:{
					'addressLine1':'',
					'addressLine2':'',
					'village':dataObj.village,
					'mandal':dataObj.mandal,
					'district':dataObj.district,
					'state':dataObj.state,
					'country':'',
					'pincode':''
				},
				principleFarmer:{
					id:'',
					name:dataObj.principleFarmer,
				}
			},
			cropStartDate:'',
			farmerID:'',
			cropName:'',
			expectedHarvestDate:'',
			expectedYield:'',
			landBase:'',
			cropInfo: [{
				'pesticides': [],
				'warehouseInformation': [],
				'irrigation': [],
				'landPreparation': [],
				'warehouseTransactionInformation': [],
				'plough': [],
				'fertilizers': [],
				'seeding': [],
				'harvest': [],
				'postHarvest': [],
				'weeding': [],
			}],
			purchaseInformation:{},//SHOULD CHNAGE AFTER ANIL CONFIRMATION
			naturalDisasters:{},//SHOULD CHNAGE AFTER ANIL CONFIRMATION
			sampleInfo:{}//SHOULD CHNAGE AFTER ANIL CONFIRMATION
		};
	};

	addFarmerCropRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'landBaseType': obj.landBaseType,
					'seedVariety': obj.seedVariety,
					'expectedYield': obj.expectedYield,
					'comments': obj.comments,
					'expectedHarvestDate': obj.expectedHarvestDate,
					'soilType': obj.soilType,
					'landBase': obj.landBase,
					'landBaseUom': obj.landBaseUom,
					'cultivationCost': '',
					'irrigationType': obj.irrigationType,
					'actualYield': obj.actualYield,
					'fieldStaffID': '',
					'soilReport': obj.soilReport,
					'cropName': obj.cropName,
					'cropSeason': obj.cropSeason,
					'cropStartDate': obj.cropStartDate,
					'totalplants': obj.totalplants,
					'yieldUom': obj.yieldUom,
					'cropLocation': obj.cropLocation
				};
	};

	addLandPreparationInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addPloughInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addIrrigationInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'irrigationType': obj.irrigationType
				};
	};

	addSeedingInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'cropSowingType':obj.cropSowingType,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addWeedingInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addHarvestingInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addPostHarvestingInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
					'labourCost': obj.labourCost,
					'fieldStaffID': '',
					'date': obj.date,
					'inputCost': obj.inputCost,
					'machinery': obj.machinery
				};
	};

	addPestisideInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
          'brand': obj.brand,
          'name': obj.name,
          'reason': obj.reason,
          'quantity': obj.quantity,
          'uom': obj.uom,
          'inputCost': obj.inputCost,
          'machinery': obj.machinery,
          'labourCost': obj.labourCost,
          'fieldStaffID': '',
          'date': obj.date,
          'impact': obj.impact,
          'infectionLevel': obj.infectionLevel,
        };
	};

	addFertilizerInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
          'labourCost': obj.labourCost,
          'fieldStaffID':'',
          'brand':obj.brand,
          'name': obj.name,
          'date': obj.date,
          'uom': obj.uom,
          'quantity': obj.quantity,
          'impact': obj.impact,
          'infectionLevel': obj.infectionLevel,
          'reason': obj.reason,
          'inputCost': obj.inputCost,
          'machinery': obj.machinery
        };
	};

	addWareHouseInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
          'warehouseId': obj.warehouseId,
          'fieldStaffID': '',
          'date': obj.date,
          'quantity': obj.quantity,
          'uom': obj.uom,
          'bondId': obj.bondId,
          'comments': obj.comments
        };
	};

	addWareHouseTransactionInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
          'warehouseId': '',
          'transactionAuthority': '',
          'date': '',
          'uom': '',
          'oldBondId': '',
          'transactiontype': '',
          'comments': '',
          'newBondId': '',
          'fieldStaffID': ''
        };
	};

	// should change below request builders
	addSamplesCollectionInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
		    'collectionDate': obj.collectionDate,
		    'cropCondition':obj.cropCondition,
		    'labReport': obj.labReport,
		    'labInformation': obj.labInformation,
          	'fieldStaffID': ''
		  };
	};

	addYieldLossInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
		    'severityLevel': obj.severityLevel,
		    'lossOfYield': obj.lossOfYield,
		    'type': obj.type,
		    'cropPeriod': obj.cropPeriod,
		    'date': obj.date,
          	'fieldStaffID': ''
		  };
	};

	addPurchaseInfoRequestBuilder = (dataObj) => {
		const obj = this.getKeyValuePairFromObj(dataObj);
		return {
			    'purchaseDate': obj.purchaseDate,
			    'purchaseAuthority': obj.purchaseAuthority,
			    'dueDate': obj.dueDate,
			    'cropGrade': obj.cropGrade,
			    'quantityUom': obj.quantityUom,
			    'quantity': obj.quantity,
			    'paidAmount': obj.paidAmount,
			    'totalAmount': obj.totalAmount,
			    'pricePerUnit': obj.pricePerUnit,
			    'dueAmount': obj.dueAmount,
          		'fieldStaffID': ''
			  };
	};

	getKeyValuePairFromObj = (obj) =>{
		if(obj !== undefined){
			var newObj = {};
			obj.forEach (function(item){
				let inobj = {
					  [item.fieldName]: item.value,
					};
				Object.assign(newObj,inobj);
			});
			return newObj;
		}
	}
}
export default RequestBuliders;