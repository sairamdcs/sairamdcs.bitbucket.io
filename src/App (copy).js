import React, { Component } from 'react';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import NavigationBar from './Navigation';
import  { FirebaseContext } from './Firebase';
import { AuthUserContext } from './Components/Session';
import Header from './Common/header.js';
import MainBody from './Common/mainBody.js';
import Footer from './Common/footer.js';
import './Assets/css/font-awesome.min.css';
import './Assets/css/bootstrap-material-design.min.css';
import './Assets/css/datatables.min.css';
import './Assets/css/datatables-select.min.css';
import './Assets/css/login.css';
import './Assets/css/styles.css';
import './Assets/css/z-media.css';
class App extends React.Component 
{
	constructor (props) {
		super(props);
		this.state = {
			isAuthenticated:false,
			loggedInUserData:{},
		}
	}
	componentDidMount = () => {
		const isAuthenticated = localStorage.getItem('isAuthenticated');
		const loggedInUserData = JSON.parse(localStorage.getItem('loggedInUserData'));
		const currentState = {...this.state};
		currentState.isAuthenticated = isAuthenticated;
		currentState.loggedInUserData = loggedInUserData;
		this.setState(currentState);
	}
	updateAuthInfo = (isAuthenticated,loggedInUserData) => {
		localStorage.setItem('isAuthenticated',isAuthenticated);
		localStorage.setItem('loggedInUserData',JSON.stringify(loggedInUserData));
		const currentState = {...this.state};
		currentState.isAuthenticated = isAuthenticated;
		currentState.loggedInUserData = loggedInUserData;
		this.setState(currentState);
	}
	logoutUser = () => {
		localStorage.setItem('isAuthenticated',"false");
		localStorage.setItem('loggedInUserData',JSON.stringify({}));
		const currentState = {...this.state};
		currentState.isAuthenticated = false;
		currentState.loggedInUserData = {};
		this.setState(currentState);
	}
	loginUser = () => {
		console.log('login clicked');
	}
  render() {
    return ( 
    	<AuthUserContext.Provider value={this.state.authUser}>
            <FirebaseContext.Consumer>
            {firebase => {
              	return <Router>

				<div className="mainFormFrame">
				<div className="overlay" />
              		<Header logoutUser={this.logoutUser} loginUser={this.loginUser} userData={this.state} firebase={firebase} />
              		<div className="wrapper max-bar">
	                <NavigationBar />
	                <MainBody updateAuthInfo={this.updateAuthInfo} userData={this.state} firebase={firebase} />
	                </div>
	                <Footer />
	            </div>
               	</Router>;
    			}
    		}
  			</FirebaseContext.Consumer>
  		</AuthUserContext.Provider>
            )
    }
}
export default App;