$(document).ready(function() {
	$(".be-connections").on("click",function(x){x.stopPropagation()})	
	$('.nav-icon').toggleClass('open');
	
/* --------------- PageHeight Start --------------------*/	
	var viewPortWidth;
	var viewPortHeight;
	
	var secondTime = 0;
	
	function getViewport() {
		if (typeof window.innerWidth != 'undefined') {
			viewPortWidth = window.innerWidth,
			viewPortHeight = window.innerHeight
		}
		else if (typeof document.documentElement != 'undefined'
			&& typeof document.documentElement.clientWidth !=
			'undefined' && document.documentElement.clientWidth != 0) {
			viewPortWidth = document.documentElement.clientWidth,
			viewPortHeight = document.documentElement.clientHeight
		}
		else {
			viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
			viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
		}
		
		return [viewPortWidth, viewPortHeight];
	}
	
	getViewport();	
	
	function setWrapperHeight(){
		getViewport();
		var wMinHeight = viewPortHeight-($(".top-bar").height());
		var cMinHeight = wMinHeight-($(".page_header").height());
		$(".wrapper").css("min-height",wMinHeight+"px");
		$(".main-content").css("min-height",cMinHeight+"px");
		if(viewPortHeight >= ($(".top-bar").height()+$(".wrapper").height())){
			$(".left-bar").css("max-height",wMinHeight+"px");
			$(".left-bar").css("height",wMinHeight+"px");
		}else{
			$(".left-bar").css("max-height",100+"%");
			$(".left-bar").css("height",100+"%");
		}
		
		$(".wrapper.mini-bar .left-bar li.submenu").each(function(){
			$(this).find("ul").css("height","auto");
			var miniMenuHeght = $(".wrapper").height()-($(this).index()*51)-40;
			if($(this).find("ul").height()>miniMenuHeght)
				$(this).find("ul").css("height",miniMenuHeght+"px");
		});
	}
	
	setWrapperHeight();
	
	window.addEventListener("resize", function() {
		setWrapperHeight();
	}, false);
	
/* --------------- PageHeight End-----------------------*/

/* ---------------- Left-Bar Start --------------------*/
	$(".submenu").find("ul").slideUp(0);
	$(".active").find("ul").slideDown(300);
	
	$('li.submenu a').click(function() {
		if($( this ).parent().hasClass( "active" )){
			$( this ).parent().find("ul").slideUp(300);
			$( this ).parent().removeClass('active');
		}else{
			if($('.wrapper').hasClass("mini-bar")){
				$('li.submenu').removeClass('active');
				$('li.submenu').find("ul").slideUp(300);
			}
			$( this ).parent().find("ul").slideDown(300);
			$( this ).parent().addClass('active');
		}
		
		if($( this ).find("i.chevron").hasClass( "ti-angle-right" )){
			$( this ).find("i.chevron").removeClass('ti-angle-right');
			$( this ).find("i.chevron").addClass('ti-angle-down');
		}else if($( this ).find("i.chevron").hasClass( "ti-angle-down" )){
			$( this ).find("i.chevron").removeClass('ti-angle-down');
			$( this ).find("i.chevron").addClass('ti-angle-right');
		}
		
	});
	
	if($('li.submenu').find("ul li").hasClass("current")){
		$('li.submenu').find("ul li.current").parent().parent().addClass('active');
		$('li.submenu').find("ul li.current").parent().slideDown(300);
	}
	
	if($( window ).width()>=700 && $( window ).width()<=1200){
		$(".wrapper").addClass('mini-bar');
		$('.wrapper').removeClass("max-bar");
		$(".wrapper .left-bar li.submenu").each(function(){
			var submenuHeight = viewPortHeight-($(this).index()*59)-45-$(".top-bar").height();
			if($(this).find("ul").height()>submenuHeight)
				$(this).find("ul").css("height",submenuHeight+"px");
		});
		$('.left-bar').css("width",40+"px");
		$('li.submenu a').parent().removeClass('active');
		$('li.submenu a').parent().find("ul").slideUp(300);
		if($('li.submenu').find("ul li").hasClass("current")){
			$('li.submenu').find("ul li.current").parent().parent().addClass('active');
			$('li.submenu').find("ul li.current").parent().slideDown(300);
		}
	}else if($( window ).width()<700){
	}else{
	}
		
	$('.showLeftPush a').click(function() { 
		$('.wrapper').toggleClass("max-bar");
		$('.wrapper').toggleClass("mini-bar");
		
		if($(".wrapper").hasClass('mini-bar')){
			$(".wrapper .left-bar li.submenu").each(function(){
				var submenuHeight = viewPortHeight-($(this).index()*59)-45-$(".top-bar").height();
				if($(this).find("ul").height()>submenuHeight)
					$(this).find("ul").css("height",submenuHeight+"px");
			});
			$('.left-bar').css("width",40+"px");
			$('li.submenu a').parent().removeClass('active');
			$('li.submenu a').parent().find("ul").slideUp(300);
			if($('li.submenu').find("ul li").hasClass("current")){
				$('li.submenu').find("ul li.current").parent().parent().addClass('active');
				$('li.submenu').find("ul li.current").parent().slideDown(300);
			}
		}else{
			$(".wrapper .left-bar li.submenu").each(function(){
				$(this).find("ul").css("height","auto");
			});
			$('.left-bar').css("width",266+"px");
			$('.wrapper').addClass("max-bar");
			$(".wrapper .left-bar li.submenu").each(function(){
				if($(this).hasClass("active")){
					$(this).find("i.chevron").removeClass("ti-angle-right").addClass("ti-angle-down");
				}else{
					$(this).find("i.chevron").removeClass("ti-angle-down").addClass("ti-angle-right");
				}
			});
		}
		
		if($(".wrapper").hasClass('mini-bar')){
			$('.nav-icon').removeClass('open');
		}
		else{
			$('.nav-icon').addClass('open');
		}
		
	});
		
	$('.menu-bar-mobile').on('click', function (e) {  
		$( ".left-bar" ).toggleClass("menu_appear" );
		$('.menu-bar-mobile').toggleClass("blur" );
		$('.mob-nav-icon').toggleClass("open" );
		$( ".overlay" ).toggleClass("show");
		$( ".top-bar" ).toggleClass("blur");
		$( ".content" ).toggleClass("blur");
		$( ".be-right-sidebar" ).toggleClass("blur");
		$( "body" ).removeClass("open-right-sidebar");
		$( ".chat" ).removeClass( "open" );
	});
	
	$(".overlay").on('click',function(){
		$( ".left-bar" ).toggleClass("menu_appear" );
		$('.menu-bar-mobile').removeClass("blur" );
		$('.mob-nav-icon').toggleClass("open" );
		$(this).removeClass("show");
		$( ".overlay" ).removeClass("show");
		$( ".top-bar" ).removeClass("blur");
		$( ".content" ).removeClass("blur");
		$( ".be-right-sidebar" ).removeClass("blur");
		$( "body" ).removeClass("open-right-sidebar");
		$( ".chat" ).removeClass( "open" );
	});
	
	$(".wrapper .left-bar li.submenu").each(function(){
		if($(this).find("ul li").hasClass("current")){
			$(this).find("i.chevron").removeClass("ti-angle-right").addClass("ti-angle-down");
		}
	});
	
/* ----------------- Left-Bar End ----------------------*/

/* --------------- Scroll Top Start ---------------------*/
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 10) {
			$('.scroll-Top').fadeIn();
			$( ".top-bar" ).addClass("smaller");
		} else {
			$('.scroll-Top').fadeOut();
			$( ".top-bar" ).removeClass("smaller");
		}
	});
	//Click event to scroll to top
	$('.scroll-Top').click(function(){
		$('html, body, .pagelefttopbg').animate({scrollTop : 0},100);
		//$('.pagelefttopbg').animate({scrollLeft : 0});
		return false;
	});
/* ----------------------- Scroll Top End ----------------*/

});

/* --------------- Bootstrap Material Design Start ---------------------*/
$(document).ready(function () {
	$('body').bootstrapMaterialDesign(); 
	$('#dtBasicExample').DataTable();
	$('.dataTables_length').addClass('bs-select');
	$('.dropdown-toggle').dropdown();
});
$(function () {
	$('[data-toggle="popover"]').popover()
	$('[data-toggle="tooltip"]').tooltip()
})

/* --------------- Bootstrap Material Design End ---------------------*/