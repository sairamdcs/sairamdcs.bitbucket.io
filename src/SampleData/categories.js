const categories =     
    [
	    {
			id: "1",
			name: "Health",
			description: "Lorem ipsum...",
			image: "assets/img/health_bg.png",
			parentCategoryId: null
	    },
	    {
			id: "2",
			name: "Food",
			description: "Lorem ipsum...",
			image: "assets/img/food_bg.png",
			parentCategoryId: null
	    },
	    {
	      	id: "3",
	      	name: "Shopping",
	      	description: "Lorem ipsum...",
	      	image: "assets/img/shop_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "4",
	        name: "Automobile",
	        description: "Lorem ipsum...",
	        image: "assets/img/automobile_bg.png",
	        parentCategoryId: null
	    },
	    {
			id: "5",
			name: "Beauty",
			description: "Lorem ipsum...",
			image: "assets/img/beauty_bg.png",
	        parentCategoryId: null
	    },
	    {
			id: "6",
			name: "Furniture",
			description: "Lorem ipsum...",
			image: "assets/img/furniture_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "7",
	        name: "Electronics",
	        description: "Lorem ipsum...",
	        image: "assets/img/electronics_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "8",
	        name: "Kids",
	        description: "Lorem ipsum...",
	        image: "assets/img/kids_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "9",
	        name: "Realestate",
	        description: "Lorem ipsum...",
	        image: "assets/img/realestate_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "10",
	        name: "Stay",
	        description: "Lorem ipsum...",
	        image: "assets/img/stay_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "11",
	        name: "Transport",
	        description: "Lorem ipsum...",
	        image: "assets/img/transport_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "12",
	        name: "Entertainment",
	        description: "Lorem ipsum...",
	        image: "assets/img/entertainment_bg.png",
	        parentCategoryId: null
	    },
	    {
	        id: "13",
	        name: "Events",
	        value:"events",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/events.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "14",
	        name: "Heritage Places",
	        value:"hindu_temple",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/heritage_places.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "15",
	        name: "Movies",
	        value:"movie_theater",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/movies.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "16",
	        name: "Museums",
	        value:'museum',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/museums.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "17",
	        name: "Play Grounds",
	        value:'campground',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/play_grounds.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "18",
	        name: "Play Stations",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/play_stations.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "19",
	        name: "Zoo",
	        value:'zoo',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/entertainment/zoo.svg",
	        parentCategoryId: "12"
	    },
	    {
	        id: "20",
	        name: "Bakers",
	        value:'bakery',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/backers.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "21",
	        name: "Bars",
	        value:'bar',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/bars.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "22",
	        name: "Buffets",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/buffets.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "23",
	        name: "Cafes",
	        value:'cafe',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/cafes.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "24",
	        name: "Drive Ins",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/drive_ins.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "25",
	        name: "Pubs",
	        value:'night_club',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/pubs.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "26",
	        name: "Restaurants",
	        value: 'restaurant',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/restaurants.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "27",
	        name: "Take Away",
	        value:"meal_takeaway",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/food/take_away.svg",
	        parentCategoryId: "2"
	    },
	    {
	        id: "29",
	        name: "Dental",
	        value:'dentist',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/dental.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "30",
	        name: "Diagnostics",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/diagnostics.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "31",
	        name: "Clinics",
	        value:'doctor',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/doctors.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "32",
	        name: "Gym",
	        value:'gym',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/gym.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "33",
	        name: "Hospitals",
	        value:'hospital',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/hospitals.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "34",
	        name: "Optical",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/optical.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "35",
	        name: "Pharmacy",
	        value:'pharmacy',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/pharmacy.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "36",
	        name: "Yoga",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/health/yoga.svg",
	        parentCategoryId: "1"
	    },
	    {
	        id: "37",
	        name: "Book",
	        value:'book_store',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/book.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "38",
	        name: "Boutique",
	        value:'clothing_store',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/boutiques.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "39",
	        name: "Cloth",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/cloth.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "40",
	        name: "Convenience store",
	        value:"convenience_store",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/convenience_store.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "40",
	        name: "Eye Wear",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/eye_wear.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "41",
	        name: "Footwear",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/footwear.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "42",
	        name: "Jewelry",
	        value:"jewelry_store",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/Jewelry.svg",
	        parentCategoryId: "3"
	    },
	    {
	        id: "43",
	        name: "Malls",
	        value:"shopping_mall",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/shops/malls.svg",
	        parentCategoryId: "3"
	    },
	    {
	      id: "44",
	      name: "Hostels",
	      description: "Lorem ipsum...",
	      image: "assets/img/icons/stay/hostels.svg",
	      parentCategoryId: "10"
	    },
	    {
	        id: "45",
	        name: "Hotel",
	        value:"lodging",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/stay/hotel.svg",
	        parentCategoryId: "10"
	    },
	    {
	        id: "46",
	        name: "Shared",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/stay/shared.svg",
	        parentCategoryId: "10"
	    },
	    {
	        id: "47",
	        name: "Airport",
	        value:"airport",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/airport.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "48",
	        name: "Auto",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/auto.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "49",
	        name: "Bus station",
	        value:"bus_station",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/bus_station.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "50",
	        name: "Metro",
	        value:"subway_station",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/metro.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "51",
	        name: "Parking",
	        value:'parking',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/parking.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "52",
	        name: "Tours travels",
	        value:"travel_agency",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/tours_travels.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "53",
	        name: "Train stations",
	        value:"train_station",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/transport/train_stations.svg",
	        parentCategoryId: "11"
	    },
	    {
	        id: "54",
	        name: "Car decors",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/automobile/car_decors.svg",
	        parentCategoryId: "4"
	    },
	    {
	        id: "55",
	        name: "Car wash",
	        value:'car_wash',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/automobile/car_wash.svg",
	        parentCategoryId: "4"
	    },
	    {
	        id: "56",
	        name: "Dealers",
	        value:"car_dealer",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/automobile/dealers.svg",
	        parentCategoryId: "4"
	    },
	    {
	        id: "57",
	        name: "Service center",
	        value:"car_repair",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/automobile/service_center.svg",
	        parentCategoryId: "4"
	    },
	    {
	        id: "58",
	        name: "Show room",
	        value:'car_dealer',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/automobile/show_room.svg",
	        parentCategoryId: "4"
	    },
	    {
	        id: "59",
	        name: "Parlors",
	        value: "beauty_salon",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/beauty/parlors.svg",
	        parentCategoryId: "5"
	    },
	    {
	        id: "60",
	        name: "Saloons",
	        value:'hair_care',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/beauty/saloons.svg",
	        parentCategoryId: "5"
	    },
	    {
	        id: "61",
	        name: "Spa",
	        value:"spa",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/beauty/spa.svg",
	        parentCategoryId: "5"
	    },
	    {
	        id: "62",
	        name: "Stores",
	        value:"stores",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/beauty/stores.svg",
	        parentCategoryId: "5"
	    },
	    {
	        id: "63",
	        name: "Bedding",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/home_furniture/bedding.svg",
	        parentCategoryId: "6"
	    },
	    {
	        id: "64",
	        name: "Dining",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/home_furniture/dining.svg",
	        parentCategoryId: "6"
	    },
	    {
	        id: "63",
	        name: "Furniture",
	        value:'furniture_store',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/home_furniture/furniture.svg",
	        parentCategoryId: "6"
	    },
	    {
	        id: "63",
	        name: "Lighting",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/home_furniture/lighting.svg",
	        parentCategoryId: "6"
	    },
	    {
	        id: "63",
	        name: "Living",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/home_furniture/living.svg",
	        parentCategoryId: "6"
	    },
	    {
	        id: "64",
	        name: "Home appliances",
	        value:'electronics_store',
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/electronics/home_appliances.svg",
	        parentCategoryId: "7"
	    },
	   {
	       id: "65",
	       name: "Laptop",
	       description: "Lorem ipsum...",
	       image: "assets/img/icons/electronics/laptop.svg",
	       parentCategoryId: "7"
	   },
	   {
	       id: "66",
	       name: "Mobile",
	       description: "Lorem ipsum...",
	       image: "assets/img/icons/electronics/mobile.svg",
	       parentCategoryId: "7"
	   },
	   {
	      id: "67",
	      name: "Commercial",
	      description: "Lorem ipsum...",
	      image: "assets/img/icons/real_estate/commercial.svg",
	      parentCategoryId: "9"
	    },
	    {
	        id: "68",
	        name: "Commercial",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/real_estate/flats.svg",
	        parentCategoryId: "9"
	    },
	    {
	        id: "69",
	        name: "Individual",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/real_estate/individual.svg",
	        parentCategoryId: "9"
	    },
	    {
	        id: "69",
	        name: "Open plots",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/real_estate/open_plots.svg",
	        parentCategoryId: "9"
	    },
	    {
	        id: "69",
	        name: "Rental",
	        description: "Lorem ipsum...",
	        image: "assets/img/icons/real_estate/rental.svg",
	        parentCategoryId: "9"
	    }
	]
export default categories