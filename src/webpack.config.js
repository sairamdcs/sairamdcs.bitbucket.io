const config = {
   entry: './main.js', // entry point
   output: {
         filename: 'index.js', // place where bundled app will be served
      },
   devServer: {
         inline: true, // autorefresh
         port: 3000, // development port server
         host: '0.0.0.0',
         disableHostCheck: true
      },
   module: {
         rules: [
            {
               test: /\.jsx?$/, // search for js files 
               exclude: /node_modules/,
               loader: 'babel-loader',
   query: {
               //presets: ['es2016', 'react'] // use es2016 and react
               presets: ['react', 'es2016'],
               plugins: ['transform-class-properties']
            }
         }
      ]
   },
   mode:'production'

}

module.exports = config;
