import React, { Component } from 'react';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './Modules/Login/index.js';
import Signup from './Modules/Signup/index.js';
import AdminApp from './Modules/AdminApp/index.js';
import * as ROUTES from './Common/Config/RouteConfig.js';
import { AuthUserContext } from './Session';
import './Assets/css/font-awesome.min.css';
import './Assets/css/bootstrap-material-design.min.css';
import './Assets/css/datatables.min.css';
import './Assets/css/datatables-select.min.css';
import './Assets/css/login.css';
import './Assets/css/styles.css';
import './Assets/css/z-media.css';
class App extends React.Component 
{
  constructor(props) {
    super(props);

    this.state = {
      authUser: null,
    };
  }

	render() {
    	return ( 
        <AuthUserContext.Provider value={this.state.authUser}>
          	<Router>
              	<div>
              		<Route path={ROUTES.LOGIN} component={Login} />
	                <Route path={ROUTES.SIGN_UP} component={Signup} />
	                <Route path={ROUTES.ADMIN_APP} component={AdminApp} />
	            </div>
           	</Router>
          </AuthUserContext.Provider>
        )
    }
}
export default App;