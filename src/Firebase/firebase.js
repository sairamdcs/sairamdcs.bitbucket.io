import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
// const config = {
//     apiKey: "AIzaSyDzwNDhnOs4E-rExmz0c-DjIoPof43tlq0",
//     authDomain: "offerly-4bc72.firebaseapp.com",
//     databaseURL: "https://offerly-4bc72.firebaseio.com",
//     projectId: "offerly-4bc72",
//     storageBucket: "offerly-4bc72.appspot.com",
//     messagingSenderId: "1068063840032"
// };
const prodConfig = {
  apiKey: "AIzaSyDzwNDhnOs4E-rExmz0c-DjIoPof43tlq0",
    authDomain: "offerly-4bc72.firebaseapp.com",
    databaseURL: "https://offerly-4bc72.firebaseio.com",
    projectId: "offerly-4bc72",
    storageBucket: "offerly-4bc72.appspot.com",
    messagingSenderId: "1068063840032"
};

const devConfig = {
  apiKey: "AIzaSyDzwNDhnOs4E-rExmz0c-DjIoPof43tlq0",
    authDomain: "offerly-4bc72.firebaseapp.com",
    databaseURL: "https://offerly-4bc72.firebaseio.com",
    projectId: "offerly-4bc72",
    storageBucket: "offerly-4bc72.appspot.com",
    messagingSenderId: "1068063840032"
};

const config =
  process.env.NODE_ENV === 'production' ? prodConfig : devConfig;

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
  }

  // *** Auth API ***

    doSignInWithEmailAndPassword = (email, password) =>
    	this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  	doPasswordUpdate = password =>
    	this.auth.currentUser.updatePassword(password);

}

export default Firebase;
