import React from 'react';
import Field from '../../../Common/Components/fields.js';
class AddNewUserComponent extends React.Component{
  	render(){
  		let obj = this.props.stateData.fields;
    	return (
   				<React.Fragment>
					<div className="row">
						<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
							<form className="form" onSubmit={this.props.saveData} noValidate >
								<div className="card-header">
									<h3>People Near Me</h3>
								</div>
								<div className="card-body people-near-me">
								
								</div>							
							</form>				
	                    </div>
					</div>
				</React.Fragment>
			);
	}
};
export default AddNewUserComponent;