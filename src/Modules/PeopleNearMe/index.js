import React from 'react';
import AddNewUserPageComponent from './container/addNewUserPageComponent.js';

class MainBody extends React.Component 
{
    render() 
	{
        return ( 
	        <AddNewUserPageComponent />
        )
    }
}


export default MainBody;