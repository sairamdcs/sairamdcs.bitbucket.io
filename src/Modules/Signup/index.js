import React from 'react';
import SignupPageComponent from './container/signupPageComponent.js';

class Signup extends React.Component 
{
    render() 
	{
        return ( 
	        <SignupPageComponent history={this.props.history} />
        )
    }
}


export default Signup;