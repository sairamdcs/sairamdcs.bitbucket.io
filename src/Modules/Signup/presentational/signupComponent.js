import React from 'react';
import * as Fields from '../../../Common/Components/index.js';
class loginComponent extends React.Component{
	
  	render(){
  		let obj = this.props.stateData.fields;
    	return (
   				<React.Fragment>
   				<div className="signupPage">
					<div className="wrapper">
						<div className="logo"><img src="images/logo.png" /></div>
						<form onSubmit={this.props.saveData} noValidate >
							<div className="form">
								<div className="form-row">
									<div className="icon"><i className="fa fa-user-o"></i></div>
									<div className="form-group col-md-12">
										<Fields.TextBox 
											fieldData={obj[0]}
											dataIndex={0}
											updateHandler={this.props.updateHandler}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="icon"><i className="fa fa-envelope-o"></i></div>
									<div className="form-group col-md-12">
										<Fields.TextBox 
											fieldData={obj[1]}
											dataIndex={1}
											updateHandler={this.props.updateHandler}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="icon"><i className="fa fa-lock"></i></div>
									<div className="form-group col-md-12">
										<Fields.TextBox 
											fieldData={obj[2]}
											dataIndex={2}
											updateHandler={this.props.updateHandler}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="icon"><i className="fa fa-lock"></i></div>
									<div className="form-group col-md-12">
										<Fields.TextBox 
											fieldData={obj[3]}
											dataIndex={3}
											updateHandler={this.props.updateHandler}
										/>
									</div>
								</div>
								<div className="nav-bar">
									<button type="submit" className="btn btn-primary btn-raised">Register</button>
								</div>
							</div>
							<div className="text-center m-t-b-15 signup">
								<div className="poweredby">&copy; Copyright and Powered by <strong>Divergent</strong></div>
							</div>
						</form>
					</div>
				</div>
				</React.Fragment>
			);
	}
};
export default loginComponent;