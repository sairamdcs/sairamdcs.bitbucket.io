import React from 'react';
import SignupComponent from '../presentational/signupComponent.js';
import * as ROUTES from '../../../Common/Config/RouteConfig.js';
var Service;
class SignupPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.FormFields = [
	        {label:'Business Name',fieldName:'first_name',value:'',labelIcon:'user-o',errorMsg:'Please enter valid Business name',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true},
	        {label:'Email',fieldName:'email',value:'',labelIcon:'envelope-o',errorMsg:'Please enter valida Email Address',validPattern:'EMAIL',validationRequired:true,isValid:true},
	        {label:'Password',fieldName:'password',value:'',labelIcon:'lock',errorMsg:'Please enter valid Password',validPattern:'PASSWORD',validationRequired:true,isValid:true},
	        {label:'Re-Enter Password',fieldName:'password_chk',value:'',labelIcon:'lock',errorMsg:'Please enter valid password',validPattern:'PASSWORD',validationRequired:true,isValid:true},
	    ];
      
		this.state = {
			fields:JSON.parse(JSON.stringify(this.FormFields)),
			isSaved:false,
			isFormValidated:false,
			showSpinner:false
		};
	}
	
	updateHandler = (e) => {
	    const formFields = this.state.fields;
	    formFields[e.target.accessKey]['value']= e.target.value;
	    this.setState({formFields:formFields});
	}
	saveData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	Service.registerNew(this.state.fields)
	    	.then((res)=>{
	    		this.props.history.push(ROUTES.STORES_MANAGEMENT);
	    		// const currentstate = this.state;
		    	// currentstate.isSaved = true;
		     //   	this.setState(currentstate);
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);

	    }
  	}

  	render(){
    	return (
    		 <SignupComponent 
   					stateData={this.state} 
   					updateHandler={this.updateHandler}
   					saveData={this.saveData}
   				/>
			
   			);
	}
};
export default SignupPageComponent;