import React from 'react';

class UsersComponent extends React.Component{
  	render(){
    	return (
   				<React.Fragment>
   				<div className="content" id="content">
                <div className="page_header">
                    <div className="pull-left">
                        <p className="text-name">
                        	<i className="fa fa-file page_header_icon" />
                        	<span className="main-text"><span>Dashboard</span></span></p>
                    </div>
                </div>
				<div className="main-content">				
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-12">
									<div className="card">
										<div className="card-header card-header-primary card-header-icon">
											<h4 className="card-title pull-left">User Management</h4>
											<span className="btn btn-primary btn-just-icon addnew pull-right" onClick={this.props.addNewUser} ><i className="fa fa-user-circle-o"></i> Add New</span>
										</div>
										<div className="card-body">
											<table id="dtBasicExample" className="table table-striped table-bordered" cellSpacing="0" width="100%">
												<thead>
													<tr>
													  <th className="th-sm">Name</th>
													  <th className="th-sm">Position</th>
													  <th className="th-sm">Office</th>
													  <th className="th-sm">Age</th>
													  <th className="th-sm">Start date</th>
													  <th className="th-sm">Salary</th>
													  <th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr role="row" className="even">
														<td className="sorting_1" tabIndex="0">Angelica Ramos</td>
														<td>Chief Executive Officer (CEO)</td>
														<td>London</td>
														<td>47</td>
														<td>2016/10/09</td>
														<td>500000</td>
														<td className="text-right">
															<a href="#" className="btn btn-link btn-success btn-just-icon edit" data-original-title="Edit"><i className="fa fa-pencil"></i></a>
															<a href="#" className="btn btn-link btn-danger btn-just-icon remove" data-original-title="Remove"><i className="fa fa-times"></i></a>
														</td>
													</tr>
													<tr role="row" className="even">
														<td className="sorting_1" tabIndex="0">Angelica Ramos</td>
														<td>Chief Executive Officer (CEO)</td>
														<td>London</td>
														<td>47</td>
														<td>2016/10/09</td>
														<td>500000</td>
														<td className="text-right">
															<a href="#" className="btn btn-link btn-success btn-just-icon edit" data-original-title="Edit"><i className="fa fa-pencil"></i></a>
															<a href="#" className="btn btn-link btn-danger btn-just-icon remove" data-original-title="Remove"><i className="fa fa-times"></i></a>
														</td>
													</tr>
													<tr role="row" className="even">
														<td className="sorting_1" tabIndex="0">Angelica Ramos</td>
														<td>Chief Executive Officer (CEO)</td>
														<td>London</td>
														<td>47</td>
														<td>2016/10/09</td>
														<td>500000</td>
														<td className="text-right">
															<a href="#" className="btn btn-link btn-success btn-just-icon edit" data-original-title="Edit"><i className="fa fa-pencil"></i></a>
															<a href="#" className="btn btn-link btn-danger btn-just-icon remove" data-original-title="Remove"><i className="fa fa-times"></i></a>
														</td>
													</tr>
												</tbody>
											</table>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				</React.Fragment>
			);
	}
};
export default UsersComponent;