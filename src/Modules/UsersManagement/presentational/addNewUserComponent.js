import React from 'react';
import * as Fields from '../../../Common/Components/index.js';
class AddNewUserComponent extends React.Component{
  	render(){
  		let obj = this.props.stateData.fields;
    	return (
				<React.Fragment>
   				<div className="modal modal-open fade show" >
					<div className="modal-dialog modal-dialog-centered" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title" id="exampleModalLongTitle">Add New User</h5>
								<button type="button" className="close" aria-label="Close" onClick={this.props.closePopup}><span aria-hidden="true">×</span></button>
							</div>
							<div className="modal-body">
						<div className="form-row">
							<div className="form-group">
								<Fields.TextBox 
									fieldData={obj[0]}
									dataIndex={0}
									updateHandler={this.props.updateHandler}
									/>
							</div>
							<div className="form-group">
								<Fields.TextBox 
									fieldData={obj[1]}
									dataIndex={1}
									updateHandler={this.props.updateHandler}
									/>
							</div>
							<div className="form-group">
								<Fields.TextBox 
									fieldData={obj[2]}
									dataIndex={2}
									updateHandler={this.props.updateHandler}
									/>
							</div>
							<div className="form-group">
								<Fields.TextBox 
									fieldData={obj[3]}
									dataIndex={3}
									multiSelect={false}
									updateHandler={this.props.updateHandler}
									/>
							</div>
							<div className="form-group">
								<Fields.SelectBox 
									fieldData={obj[4]}
									dataIndex={4}
									multiSelect={true}
									updateHandler={this.props.updateHandler}
									/>
							</div>	
							<div className="form-group">
								<Fields.SelectBox 
									fieldData={obj[5]}
									dataIndex={4}
									multiSelect={true}
									updateHandler={this.props.updateHandler}
									/>
							</div>				
						</div>
							</div>
							<div className="modal-footer">
								<Fields.PrimaryButton
									btnText="Save"
									onClickHandler={this.props.saveData}
									disabled={true} />

								<Fields.SecButton
									onClickHandler={this.props.closePopup}
									btnText="Cancel"
								 />								
							</div>
						</div>
					</div>
				</div>
				<div className="modal-backdrop fade show" />
			</React.Fragment>
		);
	}
};
export default AddNewUserComponent;