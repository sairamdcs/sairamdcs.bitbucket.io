import React from 'react';
import AddNewUserComponent from '../presentational/addNewUserComponent.js';
const FormFields = [];
var Service;
class AddNewUserPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.FormFields = [
	        {label:'Employee id',fieldName:'employeeId',value:'',fieldType:'TEXTBOX',errorMsg:'Please enter valid employee id',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Employee name',fieldName:'employeeName',value:'',fieldType:'TEXTBOX',errorMsg:'Please enter valid name',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Contact number',fieldName:'contactNumber',value:'',fieldType:'TEXTBOX',errorMsg:'Please enter valid contact number',validPattern:'NUMERIC',validationRequired:true,isValid:true},
	        {label:'Designation',fieldName:'designation',value:'',fieldType:'TEXTBOX',errorMsg:'Please enter designation',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Authorized Stores',fieldName:'authorizedStores',value:'',fieldType:'MULTISELECT',errorMsg:'Please select atleast one store',validPattern:'ARRAY',validationRequired:true,isValid:true,dropOptions:[]},
	        {label:'Authorized Permissions',fieldName:'authorizedPermissions',value:'',fieldType:'MULTISELECT',errorMsg:'Please select atleast one permission',validPattern:'ARRAY',validationRequired:true,isValid:true,dropOptions:[{key:'0',text:'User Management',value:'usermanagement'},{key:'1',text:'Store Management',value:'storeManagement'},{key:'2',text:'Offers Management',value:'offersManagement'}]},
       	];
      
		this.state = {
			fields:JSON.parse(JSON.stringify(this.FormFields)),
			isSaved:false,
			isFormValidated:false,
			showSpinner:false
		};
	}
	componentDidMount = () => {
		const stores = [{key:'0',text:'sai0',value:'sai0'},{key:'1',text:'sai1',value:'sai1'},{key:'2',text:'sai2',value:'sai2'},{key:'3',text:'sai3',value:'sai3'},{key:'4',text:'sai4',value:'sai4'}];
		const formFields = {...this.state.fields};
	    formFields[4]['dropOptions']= stores;
	    this.setState({formFields:formFields});

		//Service.getAllStores();
	}
	updateHandler = (e) => {
	    const formFields = this.state.fields;
	    formFields[e.target.accessKey]['value']= e.target.value;
	    this.setState({formFields:formFields});
	}
	onMultiSelectChangeHandler = (e,{ value }) => {
		const accessKey = e.target.parentNode.parentNode.accessKey?e.target.parentNode.parentNode.accessKey:e.target.parentNode.accessKey;
	    const formFields = this.state.fields;
	    formFields[accessKey]['value']= value;
	    this.setState({formFields:formFields});
	}
	saveData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	Service.addNewFarmer(this.state.fields)
	    	.then((res)=>{
	    		const currentstate = this.state;
		    	currentstate.isSaved = true;
		       	this.setState(currentstate);
		       	this.props.addedNewFarmerData(res.data);
	    		
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);
	    }
  	}

  	render(){
    	return (
   				<AddNewUserComponent 
   					stateData={this.state}  
   					updateHandler={this.updateHandler}
   					onMultiSelectChangeHandler={this.onMultiSelectChangeHandler}
   					saveData={this.saveData}
   					closePopup={this.props.closePopup}
   				/>
				);
	}
};
export default AddNewUserPageComponent;