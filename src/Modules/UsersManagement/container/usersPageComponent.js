import React from 'react';
import AddNewUserPageComponent from './addNewUserPageComponent.js';
import UsersComponent from '../presentational/usersComponent.js';
var Service;
class UsersPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.state = {
			showSpinner:false,
			addNewUser:false,
		};
	}
	componentDidMount = () => {
		
	}
	addNewUser = () => {
		const currentState = {...this.state};
		currentState.addNewUser = true;
		this.setState(currentState);
	}
	closePopup = () => {
		const currentState = {...this.state};
		currentState.addNewUser = false;
		this.setState(currentState);
	}
	
  	render(){
    	return (
    		<React.Fragment>
    			{this.state.addNewUser?<AddNewUserPageComponent closePopup={this.closePopup} />:null}
   				<UsersComponent 
   					stateData={this.state} 
   					updateHandler={this.updateHandler}
   					addNewUser={this.addNewUser}
   					locationHandler={this.locationHandler}
   					saveData={this.saveData}
   				/>
   				</React.Fragment>
				);
	}
};
export default UsersPageComponent;