import React from 'react';
import Field from '../../../Common/Components/fields.js';
class DashboardComponent extends React.Component{
  	render(){
    	return (
   				<React.Fragment>
					<div className="bootstrap-wrapper">
						<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
							<h3>Dashboard</h3>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
						<div className="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">						
							<div className="dashboard-card visitors-chart">
							visitors chart
							</div>
							<p>Title</p>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
	                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">						
							<div className="dashboard-card">
							Count
							</div>
							<p>Title</p>
	                    </div>
					</div>
				</React.Fragment>
			);
	}
};
export default DashboardComponent;