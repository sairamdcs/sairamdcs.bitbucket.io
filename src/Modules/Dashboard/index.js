import React from 'react';
import DashboardPageComponent from './container/dashboardPageComponent.js';

class Dashboard extends React.Component 
{
    render() 
	{
        return ( 
	        <DashboardPageComponent />
        )
    }
}


export default Dashboard;