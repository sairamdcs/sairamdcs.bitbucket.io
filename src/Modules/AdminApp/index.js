import React from 'react';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import Sidebar from '../../Common/sidebar.js';
import Header from '../../Common/header.js';
import MainBody from '../../Common/mainBody.js';
import Footer from '../../Common/footer.js';

class AdminApp extends React.Component 
{
   	render() {
    return ( 
      	<Router>
      		<div className="mainFormFrame">
				<div className="overlay" />
          		<Header />
          		<div className="wrapper max-bar">
	                <Sidebar />
	                <MainBody />
	                <Footer />
            	</div>
            </div>
       	</Router>		
        )
    }
}
export default AdminApp;