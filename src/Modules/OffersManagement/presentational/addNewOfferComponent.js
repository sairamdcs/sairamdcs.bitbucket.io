import React from 'react';
import * as Fields from '../../../Common/Components/index.js';
class AddNewOfferComponent extends React.Component{
  	render(){
  		let obj = this.props.stateData.fields;
  		let successInfo = this.props.editOffer?'Offer information Updated':'Offer information Saved';
    	return (
				<form onSubmit={this.props.editOffer?this.props.updateData:this.props.saveData} >
	   				<div className="modal modal-open fade show" >
						<div className="modal-dialog modal-dialog-centered" role="document">
							{this.props.stateData.isSaved?
								<div className="modal-content">
									<div className="modal-header">
										<h5 className="modal-title">{successInfo}</h5>
										<button type="button" className="close" aria-label="Close" onClick={this.props.closePopup}><span aria-hidden="true">×</span></button>
									</div>
									<div className="modal-footer" />
								</div>:
								<div className="modal-content">
									<div className="modal-header">
										<h5 className="modal-title">Add New Offer</h5>
										<button type="button" className="close" aria-label="Close" onClick={this.props.closePopup}><span aria-hidden="true">×</span></button>
									</div>
									<div className="modal-body">
										<div className="form-row">
											<div className="form-group row">
												<div className="col-md-6">
													<Fields.TextBox 
														fieldData={obj[0]}
														dataIndex={0}
														updateHandler={this.props.updateHandler}
														/>
												</div>
												<div className=" col-md-6">
													<Fields.TextBox 
														fieldData={obj[1]}
														dataIndex={1}
														multiSelect={false}
														updateHandler={this.props.updateHandler}
													/>
												</div>
											</div>
											<div className="form-group">
												<Fields.TextBox 
													fieldData={obj[2]}
													dataIndex={2}
													updateHandler={this.props.updateHandler}
													/>
											</div>
											<div className="form-group row">
												<div className="col-md-6">
													<Fields.DateBox 
														fieldData={obj[3]}
														dataIndex={3}
														updateHandler={this.props.updateHandler}
													/>
												</div>
												<div className=" col-md-6">
													<Fields.DateBox 
														fieldData={obj[4]}
														dataIndex={4}
														multiSelect={false}
														updateHandler={this.props.updateHandler}
													/>
												</div>
											</div>
											
											<div className="form-group">
												<Fields.SelectBox 
													fieldData={obj[5]}
													dataIndex={5}
													updateHandler={this.props.onMultiSelectChangeHandler}
													/>
											</div>
											<div className="form-group row">
												<div className="col-md-6">
													<Fields.TextBox 
														fieldData={obj[6]}
														dataIndex={6}
														updateHandler={this.props.updateHandler}
														/>
												</div>
												<div className="col-md-6">
													<Fields.TextBox 
														fieldData={obj[7]}
														dataIndex={7}
														updateHandler={this.props.updateHandler}
														/>
												</div>
											</div>
											<div className="form-group">
												<Fields.SelectBox 
													fieldData={obj[8]}
													dataIndex={8}
													multiSelect={true}
													updateHandler={this.props.onMultiSelectChangeHandler}
													/>
											</div>
											<div className="form-group row">
												<div className="col-md-6">
													<Fields.RadioGroup 
														fieldData={obj[9]}
														dataIndex={9}
														updateHandler={this.props.updateHandler}
														/>
												</div>
												<div className="col-md-6">
													<Fields.FileUpload 
														fieldData={obj[10]}
														dataIndex={10}
														handleUploadStart={this.props.handleUploadStart}
														handleUploadError={this.props.handleUploadError}
														handleUploadSuccess={this.props.handleUploadSuccess}
														handleProgress={this.props.handleProgress}
														/>
												</div>
											</div>			
										</div>
									</div>
									<div className="modal-footer">
										<Fields.PrimaryButton
											btnText="Save"
											type="submit"
											disabled={this.props.stateData.isUploading} />

										<Fields.SecButton
											onClickHandler={this.props.closePopup}
											btnText="Cancel"
										 />								
									</div>
								</div>
							}
						</div>
					</div>
				<div className="modal-backdrop fade show" />
			</form>
		);
	}
};
export default AddNewOfferComponent;