import React from 'react';

class StoresComponent extends React.Component{
  	render(){
  		const offersList = this.props.stateData.offersList;
    	return (
   				<React.Fragment>
   				<div className="content" id="content">
                <div className="page_header">
                    <div className="pull-left">
                        <p className="text-name">
                        	<i className="fa fa-file page_header_icon" />
                        	<span className="main-text"><span>Dashboard</span></span>
                        </p>
                    </div>
                </div>
				<div className="main-content">				
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-12">
									<div className="card">
										<div className="card-header card-header-primary card-header-icon">
											<h4 className="card-title pull-left">Offers Management</h4>
											<span className="btn btn-primary btn-just-icon addnew pull-right" onClick={this.props.addNewOffer} ><i className="fa fa-user-circle-o" /> Add New</span>
										</div>
										<div className="card-body">
											<table id="dtBasicExample" className="table table-striped table-bordered" cellSpacing="0" width="100%">
												<thead>
													<tr>
														<th className="th-sm">Brochure</th>
													  	<th className="th-sm">Offer name</th>
													  	<th className="th-sm">Offer type</th>
													  	<th className="th-sm">Short desc</th>
													  	<th className="th-sm">Description</th>
													  	<th className="th-sm">Start date</th>
													  	<th className="th-sm">Expiry date</th>
													  	<th>Actions</th>
													</tr>
												</thead>
												<tbody>
												{offersList.length > 0 ?
												<React.Fragment>
												{offersList.map( offer => {
													return <tr role="row" className="even" key={offer.id}>
													<td className="image"><img src={offer.brochurePath} width="{20px}" /></td>
														<td className="sorting_1">{offer.offerName}</td>
														<td>{offer.offerType}</td>
														<td>{offer.shortDesc}</td>
														<td>{offer.longDesc}</td>
														<td>{offer.startDate}</td>
														<td>{offer.expiryDate}</td>
														<td className="text-right">
															<span className="btn btn-link btn-success btn-just-icon edit" onClick={(e)=>this.props.editOffer(offer)} ><i className="fa fa-pencil" /></span>
															<span className="btn btn-link btn-danger btn-just-icon remove" onClick={(e)=>this.props.deleteOffer(offer.id)} ><i className="fa fa-times" /></span>
														</td>
													</tr>;
												})}
												</React.Fragment>: 
											<tr role="row" className="even" >
												<td colSpan="7" className="sorting_1">No Data Avaialble</td>
											</tr>
										}
												</tbody>
											</table>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				</React.Fragment>
			);
	}
};
export default StoresComponent;