import React from 'react';
import AddNewOfferPageComponent from './addNewOfferPageComponent.js';
import OffersComponent from '../presentational/offersComponent.js';
var Service;
class OffersPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.state = {
			showSpinner:false,
			addNewStore:false,
			offersList:[],
			editOfferData:{}
		};
	}
	componentDidMount = () => {
		this.getSellerOffers();
	}
	getSellerOffers = () => {
		Service.getSellerOffers()
		.then ((res)=>{
			const currentState = {...this.state};
			currentState.offersList = res;
			this.setState(currentState);
		})
		.catch ((errorObj)=>{
			console.log('errorObj',errorObj);
		});
	}
	deleteOffer = (id) => {
		Service.deleteSellerOffer(id)
		.then((res)=>{
			return this.getSellerOffers();
		})
		.catch((errorObj)=>{
			console.log('delete offer',errorObj);
		});
	}
	editOffer = (offerInfo) => {
		const currentState = {...this.state};
			currentState.editOffer = true;
			currentState.editOfferData = offerInfo;
			this.setState(currentState);
	}
	addNewOffer = () => {
		const currentState = {...this.state};
			currentState.addNewOffer = true;
			this.setState(currentState);
	}
	closePopup = () => {
		const currentState = {...this.state};
		currentState.addNewOffer = false;
		currentState.editOffer = false;
		currentState.editOfferData = {};
		this.setState(currentState);
	}
	
  	render(){
    	return (
    		<React.Fragment>
    			{this.state.addNewOffer?<AddNewOfferPageComponent getSellerOffers={this.getSellerOffers} closePopup={this.closePopup} />:null}
    			{this.state.editOffer?<AddNewOfferPageComponent getSellerOffers={this.getSellerOffers} editOffer={this.state.editOffer} editOfferData={this.state.editOfferData} closePopup={this.closePopup} />:null}
   				<OffersComponent 
   					stateData={this.state} 
   					addNewOffer={this.addNewOffer}
   					deleteOffer={this.deleteOffer}
   					editOffer={this.editOffer}
   				/>
   				</React.Fragment>
				);
	}
};
export default OffersPageComponent;