import React from 'react';
import AddNewOfferComponent from '../presentational/addNewOfferComponent.js';
import firebase from "firebase";
const FormFields = [];
var Service;
class AddNewOfferPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		var editOfferData = {};
		if(this.props.editOffer){
			editOfferData = this.props.editOfferData;
			this.offerId = editOfferData.id;
		}
		this.FormFields = [
	        {label:'Offer name',fieldName:'offerName',value:editOfferData.offerName,fieldType:'TEXTBOX',errorMsg:'Please enter offer name',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Short Description',fieldName:'shortDesc',value:editOfferData.shortDesc,fieldType:'TEXTBOX',errorMsg:'Please enter offer description',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true,maxLength:5},
	        {label:'Long Description',fieldName:'longDesc',value:editOfferData.longDesc,fieldType:'TEXTBOX',errorMsg:'Please enter offer description',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true},
	        {label:'Start Date',fieldName:'startDate',value:editOfferData.startDate,fieldType:'DATEBOX',errorMsg:'Please select atleast one store',validPattern:'DATE',validationRequired:true,isValid:true},
	        {label:'Expiry Date',fieldName:'expiryDate',value:editOfferData.expiryDate,fieldType:'DATEBOX',errorMsg:'Please select atleast one permission',validPattern:'DATE',validationRequired:true,isValid:true},

	        {label:'Offer Type',fieldName:'offerType',value:editOfferData.offerType,fieldType:'RADIOBUTTON',errorMsg:'Please offer type',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true,dropOptions:[]},
	        {label:'Minimum Percentage',fieldName:'minPercentage',value:editOfferData.minPercentage,fieldType:'TEXTBOX',errorMsg:'Please enter minimum percentage',validPattern:'FLOAT',validationRequired:true,isValid:true},
	        {label:'Maximum Percentage',fieldName:'maxPercentage',value:editOfferData.maxPercentage,fieldType:'TEXTBOX',errorMsg:'Please enter offer name',validPattern:'FLOAT',validationRequired:true,isValid:true},
	        {label:'Offer Stores',fieldName:'offerStores',value:editOfferData.offerStores,fieldType:'MULTISELECT',errorMsg:'Please select atleast one store',validPattern:'ARRAY',validationRequired:true,isValid:true,dropOptions:[]},
	        {label:'Is Best Offer',fieldName:'isBestOffer',value:editOfferData.isBestOffer?editOfferData.isBestOffer:'no',errorMsg:'Please select best offer or not',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true,options:[{label:'Yes',value:'yes'},{label:'No',value:'no'}]},
	        {label:'Upload Brochure',fieldName:'brochurePath',value:editOfferData.brochurePath,errorMsg:'Please select brochure',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:false,isValid:true,dropOptions:[]},
       	];
      
		this.state = {
			fields:JSON.parse(JSON.stringify(this.FormFields)),
			isSaved:false,
			isFormValidated:false,
			showSpinner:false,
			avatar: "",
		    isUploading: false,
		    progress: 0,
		};
	}
	componentDidMount = () => {
		const stores = [{key:'0',text:'Store0',value:'Store0'},{key:'1',text:'Store1',value:'Store1'},{key:'2',text:'Store2',value:'Store2'},{key:'3',text:'Store3',value:'Store3'},{key:'4',text:'Store4',value:'Store4'}];
		const formFields = {...this.state.fields};
	    formFields[8]['dropOptions']= stores;

	    const offerTypes = [{key:'0',text:'Discount',value:'discount'},{key:'1',text:'Cash Back',value:'cashback'},{key:'2',text:'Advertisement',value:'advertisement'}];
	    formFields[5]['dropOptions']= offerTypes;
	    this.setState({formFields:formFields});

		//Service.getAllStores();
	}
	updateHandler = (e) => {
	    const formFields = this.state.fields;
	    formFields[e.target.accessKey]['value']= e.target.value;
	    this.setState({formFields:formFields});

	}
	onMultiSelectChangeHandler = (e,{ value }) => {
		const accessKey = e.target.parentNode.parentNode.accessKey?e.target.parentNode.parentNode.accessKey:e.target.parentNode.accessKey;
	    const formFields = this.state.fields;
	    formFields[accessKey]['value']= value;
	    this.setState({formFields:formFields});
	}
	updateData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	Service.updateOffer(this.offerId,this.state.fields)
	    	.then((res)=>{
	    		const currentstate = this.state;
		    	currentstate.isSaved = true;
		       	this.setState(currentstate);
	    		this.props.getSellerOffers();
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);
	    }
  	}
	saveData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	Service.addOffer(this.state.fields)
	    	.then((res)=>{
	    		const currentstate = this.state;
		    	currentstate.isSaved = true;
		       	this.setState(currentstate);
		       	this.props.getSellerOffers();
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);
	    }
  	}
  	handleChangeUsername = event =>
    	this.setState({ username: event.target.value });
	handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
	handleProgress = progress => this.setState({ progress });
	handleUploadError = error => {
		this.setState({ isUploading: false });
		console.error(error);
	};
	handleUploadSuccess = filename => {
		this.setState({ avatar: filename, progress: 100, isUploading: false });
		firebase
			.storage()
			.ref("images")
			.child(filename)
			.getDownloadURL()
			.then((url) => {
				const currentState = {...this.state.fields};
				currentState[10]['value']= url;
				this.setState({currentState:currentState});
			});
	};

  	render(){
    	return (
   				<AddNewOfferComponent 
   					editOffer={this.props.editOffer}
   					stateData={this.state} 
   					closePopup={this.props.closePopup} 
   					updateHandler={this.updateHandler}
   					onMultiSelectChangeHandler={this.onMultiSelectChangeHandler}
   					saveData={this.saveData}
   					updateData={this.updateData}
   					handleUploadStart={this.handleUploadStart}
					handleUploadError={this.handleUploadError}
					handleUploadSuccess={this.handleUploadSuccess}
					handleProgress={this.handleProgress}
   				/>
				);
	}
};
export default AddNewOfferPageComponent;