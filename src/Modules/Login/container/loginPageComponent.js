import React from 'react';
import LoginComponent from '../presentational/loginComponent.js';
import * as ROUTES from '../../../Common/Config/RouteConfig.js';
const FormFields = [];
var Service;
class LoginPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.FormFields = [
	        {label:'Username',fieldName:'username',value:'sairam@gmail.com',labelIcon:'envelope-o',errorMsg:'Please enter username',validPattern:'EMAIL',validationRequired:true,isValid:true},
	        {label:'Password',type:'password',fieldName:'password',value:'sairam',labelIcon:'lock',errorMsg:'Please enter password',validPattern:'ALPHA_NUMERIC_SPACE',validationRequired:true,isValid:true},
	    ];
      
		this.state = {
			fields:JSON.parse(JSON.stringify(this.FormFields)),
			isSaved:false,
			isFormValidated:false,
			showSpinner:false
		};
	}
	
	updateHandler = (e) => {
	    const formFields = this.state.fields;
	    formFields[e.target.accessKey]['value']= e.target.value;
	    this.setState({formFields:formFields});
	}
	
	saveData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	const email = this.state.fields[0].value;
	    	const password = this.state.fields[1].value;
	    	this.props.firebase.doSignInWithEmailAndPassword(email, password)
		      .then(authUser => {
		      	localStorage.setItem('token',authUser.user.ra);
		      	this.props.history.push(ROUTES.DASHBOARD);
		       //this.setState({fields:JSON.parse(JSON.stringify(this.FormFields)) });
		      })
		      .catch(error => {
		      	console.log(error.message);
		        // this.setState({ error });
		      });


	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);

	    }
  	}

  	render(){
    	return (
    		<LoginComponent 
    		 		stateData={this.state} 
   					updateHandler={this.updateHandler}
   					saveData={this.saveData}
   				/>
			
   			);
	}
};
export default LoginPageComponent;