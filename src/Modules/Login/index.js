import React from 'react';
import LoginPageComponent from './container/loginPageComponent.js';
import  { FirebaseContext } from '../../Firebase';
class Login extends React.Component 
{
	constructor (props) {
		super(props);
		console.log(this.props);
	}
	render() 
	{
        return ( 
	        <FirebaseContext.Consumer>
	            {firebase => {
	    		 		return <LoginPageComponent firebase={firebase} history={this.props.history}  />
	    		 	}
    			}
  			</FirebaseContext.Consumer>
        )
    }
}


export default Login;