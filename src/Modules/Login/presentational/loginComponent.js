import React from 'react';
import * as Fields from '../../../Common/Components/index.js';
class loginComponent extends React.Component{
	
  	render(){
  		let obj = this.props.stateData.fields;
    	return (
   				<React.Fragment>
	   				<div className="loginPage">
						<div className="wrapper">
							<form onSubmit={this.props.saveData} noValidate >
								<div className="logo"><img src="images/logo.png" /></div>
								<div className="form-row">
									<div className="form-group">
										<Fields.TextBox 
											fieldData={obj[0]}
											dataIndex={0}
											updateHandler={this.props.updateHandler}
										/>
									</div>
									<div className="form-group">
										<Fields.TextBox 
											fieldData={obj[1]}
											dataIndex={1}
											updateHandler={this.props.updateHandler}
										/>
									</div>
									<div className="nav-bar">
										<button type="submit" className="btn btn-primary btn-raised">Sign In</button>
									</div>
									
								</div>
								<div className="text-center m-t-b-15 signup">
									<a href="/signup">Sign Up</a>
									<div className="poweredby">&copy; Copyright and Powered by <strong>Divergent</strong></div>
								</div>
							</form>
						</div>
					</div>
				</React.Fragment>
			);
	}
};
export default loginComponent;