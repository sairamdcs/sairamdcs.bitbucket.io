import React from 'react';

class StoresComponent extends React.Component{
  	render(){
  		const storesList = this.props.stateData.storesList;
    	return (
   				<React.Fragment>
   				<div className="content" id="content">
                <div className="page_header">
                    <div className="pull-left">
                        <p className="text-name">
                        	<i className="fa fa-file page_header_icon" />
                        	<span className="main-text"><span>Dashboard</span></span></p>
                    </div>
                </div>
				<div className="main-content">				
						<div className="container-fluid">
							<div className="row">
								<div className="col-md-12">
									<div className="card">
										<div className="card-header card-header-primary card-header-icon">
											<h4 className="card-title pull-left">All Stores</h4>
											<span className="btn btn-primary btn-just-icon addnew pull-right" onClick={this.props.addNewStore} ><i className="fa fa-user-circle-o"></i> Add New</span>
										</div>
										<div className="card-body">
											<table id="dtBasicExample" className="table table-striped table-bordered" cellSpacing="0" width="100%">
												<thead>
													<tr>
													  <th className="th-sm">Business Name</th>
													  <th className="th-sm">Brand Name</th>
													  <th className="th-sm">Category</th>
													  <th className="th-sm">Sub-Category</th>
													  
													  <th className="th-sm">Contact Name</th>
													  <th className="th-sm">Contact Number</th>
													  <th>Actions</th>
													</tr>
												</thead>
												<tbody>
												{storesList.length > 0 ?
													<React.Fragment>
													{storesList.map( store => {
														return <tr role="row" className="even" key={store.id}>
															<td className="sorting_1">{store.businessName}</td>
															<td>{store.brandName}</td>
															<td>{store.category}</td>
															<td>{store.subCategory}</td>
															<td>{store.contactName}</td>
															<td>{store.contactNumber}</td>
															<td className="text-right">
																<span className="btn btn-link btn-success btn-just-icon edit" onClick={(e)=>this.props.editStore(store)} ><i className="fa fa-pencil" /></span>
																<span className="btn btn-link btn-danger btn-just-icon remove" onClick={(e)=>this.props.deleteStore(store.id)} ><i className="fa fa-times" /></span>
															</td>
														</tr>;
													})}
													</React.Fragment>: 
													<tr role="row" className="even" >
														<td colSpan="7" className="sorting_1">No Data Avaialble</td>
													</tr>
												}
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				</React.Fragment>
			);
	}
};
export default StoresComponent;