import React from 'react';
import AddNewStorePageComponent from './addNewStorePageComponent.js';
import StoresComponent from '../presentational/storesComponent.js';
var Service;
class StoresPageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		this.state = {
			showSpinner:false,
			isLoaded:false,
			addNewStore:false,
			storesList:[],
			editStoreData:{}
		};
	}
	componentDidMount = () => {
		this.getSellerStores();
	}
	getSellerStores = () => {
		Service.getSellerStores()
		.then ((res)=>{
			const currentState = {...this.state};
			currentState.storesList = res;
			currentState.isLoaded = true;
			this.setState(currentState);
		})
		.catch ((errorObj)=>{
			console.log('errorObj',errorObj);
		});
	}
	deleteStore = (id) => {
		Service.deleteSellerStore(id)
		.then((res)=>{
			return this.getSellerStores();
		})
		.catch((errorObj)=>{
			console.log('delete store',errorObj);
		});
	}
	editStore = (storeInfo) => {
		const currentState = {...this.state};
			currentState.editStore = true;
			currentState.editStoreData = storeInfo;
			this.setState(currentState);
	}
	addNewStore = () => {
		const currentState = {...this.state};
			currentState.addNewStore = true;
			this.setState(currentState);
	}
	closePopup = () => {
		const currentState = {...this.state};
		currentState.addNewStore = false;
		currentState.editStore = false;
		currentState.editStoreData = {};
		this.setState(currentState);
	}
	
  	render(){
    	return (
    		<React.Fragment>
    			{this.state.addNewStore?<AddNewStorePageComponent closePopup={this.closePopup} getSellerStores={this.getSellerStores} />:null}
    			{this.state.editStore?<AddNewStorePageComponent editStore={this.state.editStore} editStoreData={this.state.editStoreData} closePopup={this.closePopup} getSellerStores={this.getSellerStores} />:null}
   				{this.state.isLoaded?
   				<StoresComponent 
   					stateData={this.state} 
   					addNewStore={this.addNewStore}
   					deleteStore={this.deleteStore}
   					editStore={this.editStore}
   				/>
   				:null}
			</React.Fragment>
		);
	}
};
export default StoresPageComponent;