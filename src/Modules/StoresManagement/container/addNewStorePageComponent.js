import React from 'react';
import AddNewStoreComponent from '../presentational/addNewStoreComponent.js';
import categories from '../../../SampleData/categories.js';
const FormFields = [];
var Service;
class AddNewStorePageComponent extends React.Component{
	constructor (props) {
		super(props);
		Service = global.Service;
		var editStoreData = {};
		if(this.props.editStore){
			editStoreData = this.props.editStoreData;
			this.storeId = editStoreData.id;
		}
		this.FormFields = [
	        {label:'Business name',fieldName:'businessName',value:editStoreData.businessName,fieldType:'TEXTBOX',errorMsg:'Please enter valid name',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Contact name',fieldName:'contactName',value:editStoreData.contactName,fieldType:'TEXTBOX',errorMsg:'Please enter conatct name',validPattern:'ALPHABET_SPACE',validationRequired:true,isValid:true},
	        {label:'Contact number',fieldName:'contactNumber',value:editStoreData.contactNumber,fieldType:'TEXTBOX',errorMsg:'Please enter valid contact number',validPattern:'NUMERIC',validationRequired:true,isValid:true},
	        {label:'Category',fieldName:'category',value:editStoreData.category,fieldType:'SINGLESELECT',errorMsg:'Please select atleast one category',validPattern:'NUMERIC',validationRequired:true,isValid:true,dropOptions:[]},
	        {label:'Sub-category',fieldName:'subCategory',value:editStoreData.subCategory,fieldType:'MULTISELECT',errorMsg:'Please select atleast one sub-category',validPattern:'ARRAY',validationRequired:true,isValid:true,dropOptions:[]},
	        {label:'Location',fieldName:'storeLocation',value:editStoreData.storeLocation,fieldType:'MAP_LOCATION',errorMsg:'Please select store location ',validPattern:'GEO_LOCATION',validationRequired:true,isValid:true}
       	];
      
		this.state = {
			fields:JSON.parse(JSON.stringify(this.FormFields)),
			isSaved:false,
			isFormValidated:false,
			showSpinner:false
		};
		
		this.resetState = JSON.parse(JSON.stringify(this.state));
	}
	componentDidMount = () => {
		const formFields = {...this.state.fields};
	    formFields[3]['dropOptions']= this.getCategories();
	    this.setState({formFields:formFields});
	}
	getCategories = (catId = null) => {
  		const CatObject = categories.filter((category,i)=>{
  			if(category.parentCategoryId === catId)
  			return {key:i,text:category.name,value:category.id};
  		});
  		const Cats = CatObject.map((category,i)=>{
  			return {key:i,text:category.name,value:category.id};
  		});
  		return Cats;
	}

	locationHandler = (locationObj) => {
		let formFields = this.state.fields;
	    formFields[5]['value']= {
						'location': [
						  	locationObj.lat,
						 	locationObj.lng
						]
				  	};
	    this.setState({ formFields:formFields});
	}
	updateHandler = (e) => {
	    const formFields = this.state.fields;
	    formFields[e.target.accessKey]['value']= e.target.value;
	    this.setState({formFields:formFields});
	}
	onMultiSelectChangeHandler = (e,{ value }) => {
		const accessKey = e.target.parentNode.parentNode.accessKey?e.target.parentNode.parentNode.accessKey:e.target.parentNode.accessKey;
	    const formFields = this.state.fields;
	    if(accessKey)
	    {
		    formFields[accessKey]['value']= value;
		    if (accessKey == 3){
		    	formFields[4]['dropOptions']=this.getCategories(value);
		    }
		    this.setState({formFields:formFields});
		}
	}
	updateData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	Service.updateStore(this.storeId,this.state.fields)
	    	.then((res)=>{
	    		const currentstate = this.state;
		    	currentstate.isSaved = true;
		       	this.setState(currentstate);
		       	this.props.getSellerStores();
	    		// this.props.closePopup();
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);
	    }
  	}
	saveData = (e) => {
		e.preventDefault();
	    let dataObj = this.state.fields;
	    const dataStatus = Service.validateFields.validateFieldsData(dataObj);
	    if(dataStatus.status){
	    	console.log('req',this.state.fields);
	    	Service.addStore(this.state.fields)
	    	.then((res)=>{
	    		console.log('res',res);
	    		const currentstate = JSON.parse(JSON.stringify(this.resetState));
		    	currentstate.isSaved = true;
		       	this.setState(currentstate);
		       	this.props.getSellerStores();
		       	// this.props.closePopup();
		    },(error) => {
		    	console.log(error);
		    });
	    } else {
	    	const currentstate = this.state;
	    	currentstate.fields = dataStatus.data;
	    	currentstate.isFormValidated = dataStatus.status;
	       	this.setState(currentstate);
	    }
  	}

  	render(){
    	return (
   				<AddNewStoreComponent
   					editStore={this.props.editStore}
   					closePopup={this.props.closePopup} 
   					stateData={this.state}
   					updateHandler={this.updateHandler}
   					onMultiSelectChangeHandler={this.onMultiSelectChangeHandler}
   					locationHandler={this.locationHandler}
   					saveData={this.saveData}
   					updateData={this.updateData}
   				/>
				);
	}
};
export default AddNewStorePageComponent;