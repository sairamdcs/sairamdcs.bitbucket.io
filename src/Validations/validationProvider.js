class validationProvider{
	isAlphabetSpace = (value) => {
		var re = /^[a-zA-Z ]+$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isAlphabetNumericSpace = (value) => {
		var re = /^[a-zA-Z0-9 ]*$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isNumeric = (value) => {
		var re = /^[0-9]+$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isDecimal = (value) => {
		var re = /^[0-9]+\.?[0-9]*$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isEmpty = (value) => {
		if(value === null || value.trim() === '')
			return true;
		else
			return false;
	};
	isEmptyArray = (array) => {
		if(array === null || array.length < 1)
			return true;
		else
			return false;

	}
	isValidEmail = () => {
		let regex = "^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$";
	};
	
};



export default validationProvider;