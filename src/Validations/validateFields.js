class validateFields {
	validateFieldsData = (fieldsObject) => {
		let _this = this;
			var responseData = {};
			var arr = [];
			let validationStatus = true;
			{fieldsObject.map ((item,index) => {
				if (item.validationRequired){
					let pattern = item.validPattern;
					switch (pattern) {
						case 'ALPHABET_SPACE' :
							if(!_this.isAlphabetSpace(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'ALPHA_NUMERIC_SPACE' :
							if(!_this.isAlphabetNumericSpace(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'NUMERIC' :
							if(!_this.isNumeric(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'FLOAT' :
							if(!_this.isDecimal(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'DATE' :
							if(!_this.isValidDate(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'GEO_LOCATION':
							if(!_this.isValidGeoLocation(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'ARRAY':
							if(_this.isEmptyArray(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'EMAIL':
							if(!_this.isValidEmail(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						default: 
							item.isValid = true;
							break;
					} 
					arr.push(item);
				} else {
					arr.push(item);
				}
			});
			responseData.data = arr;
			responseData.status = validationStatus;
			return responseData;
			};
	};

	isAlphabetSpace = (value) => {
		var re = /^[a-zA-Z ]+$/;
		if(value && re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isAlphabetNumericSpace = (value) => {
		var re = /^[a-zA-Z0-9 ]*$/;
		if(value && re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isNumeric = (value) => {
		var re = /^[0-9]+$/;
		if(value && re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isDecimal = (value) => {
		var re = /^[0-9]+\.?[0-9]*$/;
		if(value && re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isEmpty = (value) => {
		if(value && value.trim() === '')
			return true;
		else
			return false;
	};
	isEmptyArray = (value) => {
		if(value && value.length > 0)
			return false;
		else
			return true;
	};
	
	isValidDate = (value) => {
		if(value && value.trim() !== '')
			return true;
		else
			return false;
	};
	
	isValidGeoLocation = (value) => {
		if(value && value.location && value.location[0] && value.location[1])
			return true;
		else
			return false;
	}
	isValidEmail = (value) => {
		if(value && value.trim() !== '')
			return true;
		else
			return false;
	}
	prepareJsonObject = (dataObj) => {
		var jsonArr = {};
		for (var i = 0; i < dataObj.length; i++) {
		  jsonArr[dataObj[i].fieldName]=dataObj[i].value;
		}
		return jsonArr;
	}
	
};



export default validateFields;