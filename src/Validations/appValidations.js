class AppValidations {
	validateFieldsData = (fieldsObject) => {
		let _this = this;
		return new Promise(function(resolve, reject) {
			var responseData = {};
			var arr = [];
			let validationStatus = true;
			{fieldsObject.map ((item,index) => {
				if (item.validationRequired){
					let pattern = item.validPattern;
					switch (pattern) {
						case 'ALPHABET_SPACE' :
							if(!_this.isAlphabetSpace(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'ALPHA_NUMERIC_SPACE' :
							if(!_this.isAlphabetNumericSpace(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'NUMERIC' :
							if(!_this.isNumeric(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'DECIMAL' :
							if(!_this.isDecimal(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
						case 'EMAIL' :
							if(!_this.isValidEmail(item.value)){
								item.isValid = false;
								validationStatus = false;
							} else {
								item.isValid = true;
							}
							break;
					} 
					arr.push(item);
				} else {
					arr.push(item);
				}
			});
			responseData.data = arr;
			responseData.status = validationStatus;
			resolve(responseData);
			};
		});
	};

	isAlphabetSpace = (value) => {
		var re = /^[a-zA-Z ]+$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isAlphabetNumericSpace = (value) => {
		var re = /^[a-zA-Z0-9 ]*$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isNumeric = (value) => {
		var re = /^[0-9]+$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isDecimal = (value) => {
		var re = /^[0-9]+\.?[0-9]*$/;
		if(re.test(value) && value.trim() !== '')
			return true;
		else 
			return false;
	};

	isEmpty = (value) => {
		if(value.trim() === '')
			return true;
		else
			return false;
	};

	isValidEmail = () => {
		let regex = "^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$";
	};

	prepareJsonObject = (dataObj) => {
		var jsonArr = {};
		for (var i = 0; i < dataObj.length; i++) {
		  jsonArr[dataObj[i].fieldName]=dataObj[i].value;
		}
		return jsonArr;
	}
	
};



export default AppValidations;